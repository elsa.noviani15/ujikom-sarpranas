<?php
include "./Database.php";
class DashboardFitur extends Database{
    //Overide
    public function ambil_semua_user(){
        $hasil=$this->query("SELECT count(*) as jumlah FROM petugas WHERE terhapus=0");
        $q=mysqli_fetch_assoc($hasil);
        return $q['jumlah'];
    }
    public function ambil_user_aktif(){
        $hasil=$this->query("SELECT count(*) as jumlah FROM petugas WHERE `aktif`=1 AND terhapus=0");
        $q=mysqli_fetch_assoc($hasil);
        return $q['jumlah'];
    }
    public function ambil_data_inventaris(){
        $hasil=$this->query("SELECT sum(jumlah) as jumlah FROM inventaris WHERE id_jenis=6 AND terhapus=0");
        $q=mysqli_fetch_assoc($hasil);
        return $q['jumlah'];
    }
    public function ambil_jumlah_peminjaman(){
        $tanggal_sekarang=Date('Y-m-d');
        $hasil=0;
        $select_today=$this->query("SELECT * FROM peminjaman WHERE tanggal_pinjam LIKE'%$tanggal_sekarang%' AND tanggal_kembali='0000-00-00 00:00:00' AND terhapus=0");
        foreach ($select_today as $value) {
            $jumlah_detail=$this->query("SELECT sum(jumlah) as jumlah FROM detail_pinjam WHERE id_peminjaman='$value[id_peminjaman]' AND tanggal_kembali IS NULL AND terhapus=0");
            $q=mysqli_fetch_assoc($jumlah_detail);
            $hasil=$hasil+$q['jumlah'];
        }
        return $hasil;
    }
      public function ambil_semua_inventaris(){
        $hasil=$this->query("SELECT sum(jumlah) as jumlah FROM inventaris WHERE terhapus=0");
        $q=mysqli_fetch_assoc($hasil);
        return $q['jumlah'];
    }
};