<?php
include"connect.php";
session_start()
?>
<!DOCTYPE html>
<html lang="en">
<head>      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
  <title>Sarana Dan Prasarana SMKN 1 Ciomas</title>
  <link rel="icon" href="images/S.png" sizes="32x32">
  <!-- CSS  -->
  <link href="css/index/css/font.css?family=Material+Icons" rel="stylesheet">
  <link href="css/index/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="css/index/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style type="text/css">
  .modal-heading {
    width: 100%;
    padding: 13px 13px 13px 26px;
    border-bottom: 1px solid #9e9e9ea1;
    background: #00bcd4;
    color: #fff;
  }
  .modal-heading > h5{
    font-size: 22px;
  }
  #modal1{
    margin-top: 54px;
  }
  footer{
    margin-left:0;
  }
</style>

</head>
<body>
  <nav class="nav-index" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo"><img src="images/SG.png" style="width:30px;"><span>arpranas</span></a>
      <ul class="right">
        <li>
         <a class="btn waves-effect waves-light blue" id="sign_in" onclick="OpenModal('Login','sign_in.php')"><span>Sign In</span></a>
       </li>
       <li> <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
          <a id="help" onclick="OpenModal('Help','help.php')" class="btn-floating btn-large red">?</a>
         </div>
         </li>
     </ul>
   </nav>

   <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center teal-text text-lighten-2">Sarpranas</h1>
        <div class="row center">
          <h5 class="header col s12 light">Inventarisasi Sarana dan Prasarana Sekolah</h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="images/office.jpg" alt="Unsplashed background img 1"></div>
  </div>


  <div class="container">
    <div class="section">
      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">flash_on</i></h2>
            <h5 class="center">Sarpranas</h5>

            <p class="light" align="justify">Aplikasi Inventarisasi sarana dan prasaran ini digunakan untuk mempermudah pendataan asset,pendataan pemasukan,kehilangan dan kerusakan terhadap suatu asset yang dapat dilihat secara real time, sehingga dapat meminimalisir sebuah kehilangan maupun pihak-pihak yang memanfaatkan keamanan yang kurang akibat pendataan yang belum tersusun secara baik.</p>
          </div>
        </div>

        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">group</i></h2>
            <h5 class="center">SMKN 1 CIOMAS</h5>

            <p class="light" align="justify">SMK Negeri 1 Ciomas Adalah Sekolah Negeri Yang Berdiri Pada Tahun 2008 Di Jalan Raya Laladon, Desa Laladon, Kecamatan Ciomas. SMK Negeri 1 Ciomas Memiliki 4 Jurusan Yaitu RPL, Animasi, TKR, TPL dan di tambah lagi satu jurusan yaitu broadcasting.

Siswa Di SMK Negeri 1 Ciomas di latih untuk bersikap jujur, disiplin, taat, dan mencintai lingkungan sekitar. Diantaranya program yang ada di sekolah kita adalah Gerakan Disiplin Sekolah(GDS), Jum'at Taqwa, Literasi, Dan Masih Banyak Lagi.</p>
          </div>
        </div>


    </div>
  </div>


  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light">Aplikasi Sistem sarana dan prasarana sekolah </h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="images/LAB.jpg" alt="Unsplashed background img 2"></div>
  </div>
  <div class="container">
    <div class="section">

      <div class="row">
        <div class="col s12 center">
          <h3><i class="mdi-content-send brown-text"></i></h3>
          <h4>Tentang SMKN 1 CIOMAS</h4>
          <p class="left-align light">SMK Negeri 1 Ciomas Adalah Sekolah Negeri Yang Berdiri Pada Tahun 2008 Di Jalan Raya Laladon, Desa Laladon, Kecamatan Ciomas. SMK Negeri 1 Ciomas Memiliki 4 Jurusan Yaitu RPL, Animasi, TKR, TPL dan di tambah lagi satu jurusan yaitu broadcasting.

Siswa Di SMK Negeri 1 Ciomas di latih untuk bersikap jujur, disiplin, taat, dan mencintai lingkungan sekitar. Diantaranya program yang ada di sekolah kita adalah Gerakan Disiplin Sekolah(GDS), Jum'at Taqwa, Literasi, Dan Masih Banyak Lagi.</p>
        </div>
      </div>

    </div>
  </div>
  <button id="showToast" style="display:none" onclick="Materialize.toast('<?=$_SESSION['message'];?>', 4000)"></button>
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d441.7581812244595!2d106.7588807063765!3d-6.585933953620855!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c4e2d5943ecf%3A0xb138f2dbc041f8f5!2sSMK+Negeri+1+Ciomas!5e1!3m2!1sid!2sid!4v1554339869741!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
    <footer class="page-footer blue">
    <div class="container">
      <div class="row">
        <div class="col md6">
          <h5 class="white-text">Sarpranas</h5>
          <p class="grey-text text-lighten-4">Aplikasi Inventarisasi sarana dan prasaran ini digunakan untuk mempermudah pendataan asset,pendataan pemasukan,kehilangan dan kerusakan terhadap suatu asset yang dapat dilihat secara real time, sehingga dapat meminimalisir sebuah kehilangan maupun pihak-pihak yang memanfaatkan keamanan yang kurang akibat pendataan yang belum tersusun secara baik.</p>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container center">
        Uji Kompetensi 2019 <a class="brown-text text-lighten-3" href="#">Sarpranas</a>
      </div>
    </div>
  </footer>
  <button class="modal-trigger hidden" style="display:none" id="openModal" href="#modal1">open</button>
  <div id="modal1" class="modal">
    <div class="modal-heading">
      <h5 id="title"></h5>
    </div>
    <div class="modal-content">
      <div class="content"></div>
    </div>
    <a href="#" id="close-button" class="modal-close hidden"></a>
  </div>


  <!--  Scripts-->
  <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
  <!--materialize js-->
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script src="css/index/js/init.js"></script>
  <script type="text/javascript" src="js/custom-script.js"></script>
  <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
  <script type="text/javascript" src="js/plugins.min.js"></script>
</body>
</html>
<?php
if(isset($_SESSION['message'])){
  echo"<script>document.getElementById('showToast').click();</script>";
  unset($_SESSION['message']);
}
if(isset($_GET['mode']) && $_GET['mode']=='ubah-password'){
  echo"<script>setTimeout(function(){OpenModal('Forgot Password','form_new_pass.php?token=$_GET[token]')},10)</script>";
}
?>