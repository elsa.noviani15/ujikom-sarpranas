DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(15) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(15) NOT NULL,
  `jumlah` varchar(50) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(15) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(500) NOT NULL,
  `keterangan` varchar(500) NOT NULL,
  `jumlah` varchar(50) NOT NULL,
  `id_jenis` int(15) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` int(15) NOT NULL,
  `kode_inventaris` varchar(15) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("10","obeng","baik","jhsd2","22","34","2019-02-15 14:44:10","2","2","0");
INSERT INTO inventaris VALUES("12","tang","baik","pel pro","21","2","2019-02-17 08:44:42","12","1111","0");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(15) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(15) NOT NULL,
  `keterangan` varchar(500) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(15) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","petugas");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(15) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(20) NOT NULL,
  `nip` int(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","annisa","1234","bogor","3");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(15) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(50) NOT NULL,
  `id_pegawai` int(15) NOT NULL,
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_petugas` varchar(20) NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=12357 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("12345","annisa","annisa","annisa","12345");
INSERT INTO petugas VALUES("12355","annisa","nisa","unangggg","1");
INSERT INTO petugas VALUES("12356","annisa","nisa","unang","2");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(15) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(20) NOT NULL,
  `kode_ruang` int(15) NOT NULL,
  `keterangan` varchar(500) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




