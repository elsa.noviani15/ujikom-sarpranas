<?php
  include "../Database.php";
?>
<form id="formModal" method="POST" action="./proses_sign.php" class="col s12">
  <div class="row">
    <div class="input-field col s12">
      <input type="text" name="username" class="validate" required="" autocomplete="false" autofocus>
      <label for="key">Username</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="password" name="password" class="validate" required="" autocomplete="false">
      <label for="value">Password</label>
    </div>
  </div>
  <div class="row">
  	<div class="col m3">
       <a id="forgot_pass" style="cursor:pointer" onclick="OpenModal('Forgot Password','forgot_pass.php')"><span>Forgot Password?</span></a>
    </div>
  	<div class="col m9">
  		<input type="submit" name="submit" value="Login" class="btn blue right ml-10">
  		<button type="button" href="#" class="btn red right" onclick="CloseModal()">Cancel</button>
  	</div>
  </div>
</form>

