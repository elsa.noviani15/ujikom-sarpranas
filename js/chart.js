var PieDoughnutChartSampleData = [
    {
        value: 300,
        color:"#F7464A",
        highlight: "#FF5A5E",
        label: "Red"
    },
    {
        value: 50,
        color: "#46BFBD",
        highlight: "#5AD3D1",
        label: "Green"
    },
    {
        value: 100,
        color: "#FDB45C",
        highlight: "#FFC870",
        label: "Yellow"
    }
]
$.ajax({
    type:'get',
    url:'./chart.php?mode=total-hari-ini',
    success:function(data){
        console.log(data);
        
        PieDoughnutChartSampleData=data;
    }
})
 window.onload = function() {
  window.PieChartSample = new Chart(document.getElementById("total-pinjam-hari-ini").getContext("2d")).Pie(PieDoughnutChartSampleData,{
   responsive:true
  });
 };