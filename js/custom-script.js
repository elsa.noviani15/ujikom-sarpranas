﻿// Ini FUngsi buat nampilin modal
var OpenModal=function(title,url){
	$(".content").load(url + " " + '#formModal', function (response, status) {
  		$('select').material_select();
	});
	$('#title')[0].innerHTML=title;
	$("#openModal").click();
}
// ini fungsu tutup modal
var CloseModal=function(){
	$('#close-button').click();
}
// ini fungsi buat munculin alert saat mau delete data
var showDialogDelete=function(url){
	swal({    
		title: "Apakah Anda Yakin Ingin Menghapus Data?",
		text: "Anda Tidak Akan Bisa Kembalikan Data Ini Kembali!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Ya, Hapus!",   
		closeOnConfirm: false 
	}, function(){   
		// swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
		window.location=url;
	})
}
var showDialogLogout=function(url){
	swal({    
		title: "Apakah Anda Yakin Ingin Keluar?",
		text: "Anda Akan Keluar Dari Halaman Website Ini!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Keluar",   
		closeOnConfirm: false 
	}, function(){   
		window.location=url;
	})
}
var removeParam =function(param) {
        var url = window.location.href.split('?')[0]+'?';
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
     
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] != param) {
                url = url + sParameterName[0] + '=' + sParameterName[1] + '&'
            }
        }
        setTimeout(function(){
    		window.location=url.substring(0,url.length-1);
        },2000)
}
// Dropdown Menu
$(".dropdown-content.select-dropdown li").on( "click", function() {
	var that = this;
	setTimeout(function(){
		if($(that).parent().hasClass('active')){
			$(that).parent().removeClass('active');
			$(that).parent().hide();
		}
	},100);
});
