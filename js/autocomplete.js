var data=[]; //Inisialisasi variable data
// Ini Buuat Auto Complete
$(function() {
$('#showBtnPinjam').hide(); //jquery untuk hide element 
    var index=1; //inisialisasi variable index
    // auto complete di element kode
    $("#kode").autocomplete({
            delay:0,
            source: "autocomplete.php",
            minLength: 3,
            // select: function( event, ui ) {
            //     gambar(ui.item.foto); 
            // }
            select: function( event, ui ) {
                $('#showBtnPinjam').show(); //saat udh pilih di quto complete muncuk button
                var index=data.findIndex(x=>x.id_inventaris === ui.item.id_inventaris) // untuk mencari index dalam array data
                if(index !== -1){
                    data[index].jumlah++; //menambah nilai jumlah dalam array data
                }else{
                    if(parseInt(ui.item.stok) > 0){
                        data.push(ui.item);
                    }else{
                        Materialize.toast('Maaf stok tidak tersedia', 4000); //munculkan notifikasi
                    }
                }
                showTable(true); // trigger function show table dengan parameter true
                setTimeout(() => {
                    document.getElementById('kode').value='';
                }, 100);
            }
    });
}); 
// Ini bua handle klik icon tong sampah
function deletes(index){ //paramter diisi dengan index arraynya
    data.splice(index,1);
    showTable(); //trigger function showtable tanpa parameter
}
// Ini Buat Nampilihn Row dalam table
function CountBarang(back,index){ 
    var count=0;
    if(back==false){data[index].jumlah=parseInt($('#jumlah_'+index)[0].value)};
	for (var i = 0; i < data.length; i++) {
        if(data[i].stok !=0){
            count +=data[i].jumlah;
        }
	}
	if(back==false){
		if(data[index].stok < parseInt($('#jumlah_'+index)[0].value)){
			$('#alert_'+index).show();
    		$('#btn-submit').prop('disabled', true);
    		$('#kode').prop('readonly', true);
    		$('#kode').prop('title', "Silahkan Masukan Jumlah yang benar Sebelum Memilih barang yang lain"); 
		}else{
			$('#alert_'+index).hide();
    		$('#btn-submit').prop('disabled', false);
    		$('#btn-submit').prop('title', "Pinjam");
    		$('#kode').prop('readonly', false);
    		$('#kode').prop('title', "Masukan Nama atau ID barang");
		}
		$('#jumlah_pinjam')[0].innerHTML=count;
	}else{
		return count;
	};	
}
function showTable(add) {
    var table = '';
    for (let i = 0; i < data.length; i++) {
        var disabled=data[i].stok == 0?'disabled':'';
        table += '<tr id="index_' + i + '">'
        table = table + '<td class="center">' + (i+1) + '</td>'
        table = table + '<td>' + data[i].label + '</td>'
        table = table + '<td><span class="right">' + data[i].stok + '</span></td>'
        table = table + '<td><input id="jumlah_'+i+'" type="number" min="'+(data[i].stok==0?0:1)+'" max="' + data[i].stok + '"'+ 
        				'value="' + (data[i].stok==0?0:data[i].jumlah) + '" oninput="CountBarang(false,'+i+')" class="form-control mb-0 text-right"' + disabled+'>'+
        				'<label class="label-alert" id="alert_'+i+'"><i class="mdi-alert-warning"></i> Jumlah Melebihi Stok yang ada</label></td>';
        table = table +'<td class="center"><i class="mdi-action-delete red-text" onclick="deletes('+i+')" style="cursor:pointer"></i></td>'
        tabel = table + '</tr>'
    }
    if (data.length == 0) {
    	$('#Datanotfound').show();
    	$('#showBtnPinjam').hide();
 	} else {
    	$('#showBtnPinjam').show();
 		$('#Datanotfound').hide();
    	table = table + '<tr id="totalBarang"><td colspan="4"><span class="right">Total Barang :</span></td><td colspan="2"><span class="right" id="jumlah_pinjam">' +CountBarang(true)+'</span></td></tr>'
 	};
    $('#showData').html(table); // isi element showdata dengan variable table       
    if (add) {
        document.getElementById('kode').blur();
        if (data[data.length-1] !=0){
            setTimeout(() => {
                document.getElementById('jumlah_' + (data.length - 1)).focus()
            }, 100);
        }
    }
}