<?php 
include 'page/Database.php'; 
$config=new Database();
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
session_start();
if(isset($_SESSION['level'])){ 
  if($_SESSION['level']==2){
    header("location:Dashboard_Op.php");
  }else if($_SESSION['level']==1){
    header("location:Dashboard.php");
  }else{
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <!--   <base href="<?=$config->base_url;?>"> -->
      <meta name="description" content="Uji Kompetensi Sarana Dan Prasarana SMKN 1 Ciomas">
      <meta name="keywords" content="Sarana Dan Prasarana SMKN 1 Ciomas">
      <title>Sarana Dan Prasarana SMKN 1 Ciomas</title>

      <!-- Favicons-->
      <link rel="icon" href="images/S.png" sizes="32x32">
      <!-- Favicons-->
      <link rel="apple-touch-icon-precomposed" href="images/S.png">
      <!-- For iPhone -->
      <meta name="msapplication-TileColor" content="#00bcd4">
      <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
      <!-- For Windows Phone -->


      <!-- CORE CSS-->
      <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
      <link href="css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">

      <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
      <link href="plugins/animate-css/animate.css" rel="stylesheet" />

      <link href="js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
      <link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
      <link href="js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
      <link href="js/plugins/sweetalert/sweetalert.css" type="text/css" rel="stylesheet" media="screen,projection">
      <!-- <link href="js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection"> -->
      <!-- Custome CSS-->    
      <link href="css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection"> 
      <link href="css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">  
      <link href="js/jquery-ui.min.css" rel="stylesheet">
      <link href="plugins/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css" type="text/css" rel="stylesheet" media="screen,projection">
      <link href="plugins/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    </head>

    <body>
      <!-- Start Page Loading -->
      <div id="loader-wrapper">
        <div id="loader"></div>        
        <!-- <div class="loader-section section-left"></div> -->
        <!-- <div class="loader-section section-right"></div> -->
      </div>
      <!-- End Page Loading -->
      <!-- Modal -->
      <button class="modal-trigger hidden" style="display:none" id="openModal" href="#modal1">open</button>
      <div id="modal1" class="modal">
        <div class="modal-heading">
          <h5 id="title"></h5>
        </div>
        <div class="modal-content">
          <div class="content"></div>
        </div>
        <a href="#" id="close-button" style="display:none" class="modal-close hidden"></a>
      </div>
      <!-- Close -->
    <!-- ================================================
    Scripts
    ================================================ -->
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--prism-->
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="plugins/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>   
    <script type="text/javascript" src="js/custom-script.js"></script>
    <script type="text/javascript" src="js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/data-tables/data-tables-script.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert/sweetalert.min.js"></script>   
    <!-- START HEADER -->
    <header id="header" class="page-topbar">  
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color">
          <div class="nav-wrapper">
            <ul class="left">                      
              <li><h1 class="logo-wrapper"><a href="Dashboard.php" class="brand-logo darken-1"><img src="images/SG.png" style="width:30px;"><span>arpranas</span></a></h1></li>
            </ul>
            <ul class="right hide-on-med-and-down">
              <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
              </li>                      
              <li><a href="#" onclick="showDialogLogout('sign_out.php')"><i class="mdi-hardware-keyboard-tab"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main" style="padding-left: 0;">
      <div class="owl-carousel owl-theme" id="owl-carousel">
        <div class="item" style="height:400px"><img src="images/smkbisa.jpg"></div>
        <div class="item" style="height:400px"><img src="images/ppa.jpg"></div>
        <div class="item" style="height:400px"><img src="images/LAB.jpg"></div>
        <div class="item" style="height:400px"><img src="images/office.jpg"></div>
      </div>
      <div class="card container" style="margin-top: -162px; z-index: 9999;width:87%; ">
       <div class="row">
        <div class="col s12 m4 center" style="padding: 17px 0 0 0;">
          <img src="images/user.png" style="width:30%;border-radius: 100%;">
          <p>
            Nama Pengguna : <?=$_SESSION['username'];?>
            <br>
            Email : <?=$_SESSION['email'];?>
            <br>
            Status : Aktif
            <br>
            Level : Siswa
          </p>
        </div>
        <div class="col s12 m8">

          <ul class="collapsible popout"  data-collapsible="accordion">

            <li>
              <div class="collapsible-header #1e88e5 blue darken-1 white-text"><i class="mdi-communication-message"></i>Sarpranas</div>
              <div class="collapsible-body">
                <p align="justify">Inventarisasi sarana dan prasarana sekolah adalah salah satu aktivitas kerja yang sangat dibutuhkan oleh sekolah , aktivitas tersebut dapat membantu berlangsungnya kegiatan pendidikan yang ada di sekolah , khususnya sekolah Ar Rafi. Di sekolah Ar Rafi saat ini aktivitas inventarisasi seperti pencatatan barang, penempatan barang,keluar masuk barang, serta pengecekan kondisi atau stok barang masih menggunakan formulir manual , bahkan ada kegiatan pengelolaan inventaris yang belum dilaksanakan.Dari permasalahan tersebut maka muncul gagasan untuk membuat suatu aplikasi berbasis web, yang di dalamnya dapat melakukan aktivitas-aktivitas inventarisasi dengan mudah. Metodelogi yang di butuhkan dalam penyelesaian aplikasi ini adalah Prototype dengan Bahasa pemogramman PHP dan HTML, serta database Mysql.Dengan tersedianya jaringan internet atau intranet (internet lokal) di dalam sekolah Ar Rafi . Aplikasi ini dapat digunakan sebagai media atau pengontrol segala sesuatu yang berhubungan dengan inventaris sekolah.
                </p>
              </div>
            </li>

            <li>
              <div class="collapsible-header  #2196f3 blue white-text"><i class="mdi-communication-clear-all"></i>Cara Peminjaman Alat</div>
              <div class="collapsible-body lime lighten-5">
                <h5 align="center">Cara Peminjaman</h5>
                <ul class="container">
                  <li>1. Klik Menu Form Peminjaman pada Dashboard </li>
                  <li>2. “Cari berdasarkan barang atau ID” ketik  alat-alat yang ingin di pinjam</li>
                  <li>3. Klik nama alat yang telah dipilih</li>
                  <li>4. Tuliskan jumlah barang yang ingin dipinjam</li>
                  <li>5. Klik Button “Pinjam” apabila telah selesai meminjam barang </li>
                  <li>6. Klik Icon Trash (Tempat Sampah) Apabila anda salah memilih nama alat, atau tidak ingin meminjamnya</li>
                  <li><b>*Catatan : Apabila meminjam lebih dari 1 tuliskan nama inventaris pada form input “Cari berdasarkan barang atau ID” kembali.</b></li>
                </ul>
              </div>
            </li>
            <li>
              <div class="collapsible-header #64b5f6 blue lighten-2 white-text"><i class="mdi-action-list"></i> Form Peminjaman</div>
              <div class="collapsible-body">
                <section id="content">

                  <!--breadcrumbs start-->
                  <div id="breadcrumbs-wrapper">
                    <!-- Search for small screen -->
                    <div class="header-search-wrapper grey hide-on-large-only">
                      <i class="mdi-action-search active"></i>
                      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
                    </div>
                  </div>
                  <!--breadcrumbs end-->

                  <button id="showToast" style="display:none" onclick="Materialize.toast('<?=$_SESSION['message'];?>', 4000)"></button>
                  <!--start container-->
                  <div class="container">
                    <?php
                    $page = $_GET['page'];
                    if ($page == "inventaris") {
                      include "page/inventaris/inventaris.php";
                    }else
                    {
                      ?>
                      <div class="row">
                        <div class="section pt-0 col m12 s12 pb-18">
                          <h4 class="card-header">Peminjaman</h4>
                          <div class="container mt-10 mb-10 table-responsive">
                            <input type="text" name="NamaBarang" class="form-control custom-control" placeholder="Cari Berdasarkan Nama Barang atau ID" id="kode" autofocus=""> 
                            <table class="display bordered custom-table" cellspacing="1">
                              <thead>
                                <tr>
                                  <th class="wd-44 center">No</th>
                                  <th>Nama Inventaris</th>
                                  <th>Stok</th>
                                  <th>Jumlah</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody id="showData">
                              </tbody>
                              <tbody id="Datanotfound">
                                <tr>
                                  <td colspan="5" class="center">Tabel Kosong</td>
                                </tr>
                              </tbody>
                            </table>
                            <div class="form-group" id="showBtnPinjam">
                              <button class="btn btn-custom right" id="btn-submit">Pinjam</button>
                            </div>
                          </div>
                        </div>
                        <div class="section card pt-0 col m4 s12">
                  <!-- <h4 class="card-header">Total Peminjam Hari Ini</h4>
                <div class="container mt-10">
                <div class="col s12 m4 l2">
                            <p class="z-depth-5 shadow-demo"></p>
                          </div>
                        </div> -->
                      </div>
                    </div>
                    <?php
                  }
                  ?>
                  <!-- Floating Action Button -->
                  <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
                   <a href="Helper.htm "class="btn-floating btn-large"><i class="mdi-communication-live-help"></i></a>
                 </div>
                 <!-- Floating Action Button -->
               </div>
               <!--end container-->
             </section>
           </div>
         </li>
                  <!-- <li>
                    <div class="collapsible-header lime darken-4 white-text"><i class="mdi-device-bluetooth-searching"></i> Forth Header</div>
                    <div class="collapsible-body lime lighten-5">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                      </p>
                    </div>
                  </li>
                  <li>
                    <div class="collapsible-header orange white-text"><i class="mdi-device-now-widgets"></i> Fifth Header</div>
                    <div class="collapsible-body orange lighten-5">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                      </p>
                    </div>
                  </li> -->
                </ul>

              </div>
            </div>
          </div>
        </div>
      </div>
      
      <!-- END WRAPPER -->

    </div>

    <script type="text/javascript" src="js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="js/export/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="js/export/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="js/plugins.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/autocomplete.js"></script>

    <script>
     $(document).ready(function () {
       $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        autoplay:true,
        nav: true,
        autoHeight:true,
        responsive: {
         0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 1
        }
      }
    })
     })
   </script>
   <script type="text/javascript" src="js/proses_pinjam.js"> </script> 
 </body>
 <?php
 include'footer.php';
 echo "</html>";
}
}else
{
  header('location:index.php');
}
?>