<?php 
include 'page/Database.php'; 
$config=new Database();
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
session_start();
if(isset($_SESSION['level'])){ 
  if($_SESSION['level']==3){
    header("location:Dashboard_Siswa.php");
  }else if($_SESSION['level']==1){
    header("location:Dashboard.php");
  }else{
  ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <!--   <base href="<?=$config->base_url;?>"> -->
      <meta name="description" content="Uji Kompetensi Sarana Dan Prasarana SMKN 1 Ciomas">
      <meta name="keywords" content="Sarana Dan Prasarana SMKN 1 Ciomas">
      <title>Sarana Dan Prasarana SMKN 1 Ciomas</title>

      <!-- Favicons-->
      <link rel="icon" href="images/S.png" sizes="32x32">
      <!-- Favicons-->
      <link rel="apple-touch-icon-precomposed" href="images/S.png">
      <!-- For iPhone -->
      <meta name="msapplication-TileColor" content="#00bcd4">
      <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
      <!-- For Windows Phone -->


      <!-- CORE CSS-->
      <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
      <link href="css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">

      <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
      <link href="plugins/animate-css/animate.css" rel="stylesheet" />

      <link href="js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
      <link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
      <link href="js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
      <link href="js/plugins/sweetalert/sweetalert.css" type="text/css" rel="stylesheet" media="screen,projection">
      <!-- <link href="js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection"> -->
      <!-- Custome CSS-->    
      <link href="css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection"> 
      <link href="css/custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">  
      <link href="js/jquery-ui.min.css" rel="stylesheet">
    </head>

    <body>
      <!-- Start Page Loading -->
      <div id="loader-wrapper">
        <div id="loader"></div>        
        <!-- <div class="loader-section section-left"></div> -->
        <!-- <div class="loader-section section-right"></div> -->
      </div>
      <!-- End Page Loading -->
      <!-- Modal -->
      <button class="modal-trigger hidden" style="display:none" id="openModal" href="#modal1">open</button>
      <div id="modal1" class="modal">
        <div class="modal-heading">
          <h5 id="title"></h5>
        </div>
        <div class="modal-content">
          <div class="content"></div>
        </div>
        <a href="#" id="close-button" style="display:none" class="modal-close hidden"></a>
      </div>
      <!-- Close -->
    <!-- ================================================
    Scripts
    ================================================ -->
    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--prism-->
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="js/custom-script.js"></script>
    <script type="text/javascript" src="js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/data-tables/data-tables-script.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert/sweetalert.min.js"></script>   
    <!-- START HEADER -->
    <header id="header" class="page-topbar">  
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color">
          <div class="nav-wrapper">
            <ul class="left">                      
              <li><h1 class="logo-wrapper"><a href="Dashboard_OP.php" class="brand-logo darken-1"><img src="images/SG.png" style="width:30px;"><span>arpranas</span></a></h1></li>
            </ul>
            <ul class="right hide-on-med-and-down">
              <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
              </li>                      
              <li><a href="#" onclick="showDialogLogout('sign_out.php')"><i class="mdi-hardware-keyboard-tab"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <!-- end header nav-->
    </header>
    <!-- END HEADER -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">

        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav">
          <ul id="slide-out" class="side-nav fixed leftside-navigation">
            <li class="user-details cyan darken-2">
              <div class="row">
                <div class="col col s4 m4 l4">
                  <img src="images/user.png" alt="" class="circle responsive-img valign profile-image">
                </div>
                <div class="col col s8 m8 l8">
                  <ul id="profile-dropdown" class="dropdown-content">
                    <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                    </li>
                  </ul>
                  <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><?=$_SESSION['username'];?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                  <p class="user-roal">Operator</p>
                </div>
              </div>
            </li>
            <li class="bold"><a href="Dashboard_OP.php" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
            </li>
            <li class="li-hover"><div class="divider"></div></li>
            <li class="no-padding">
              <ul class="collapsible collapsible-accordion">
                <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-action-view-list"></i>Data Peminjaman</a>
                  <div class="collapsible-body">
                    <ul>
                      <li><a href="Dashboard_OP.php?page=peminjaman">Peminjaman Inventaris</a>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
          </li>
      </ul>
        <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->
        <section id="content">

          <!--breadcrumbs start-->
          <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
              <i class="mdi-action-search active"></i>
              <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          </div>
          <!--breadcrumbs end-->

          <button id="showToast" style="display:none" onclick="Materialize.toast('<?=$_SESSION['message'];?>', 4000)"></button>
          <!--start container-->
          <div class="container">
            <?php
            $page = $_GET['page'];
            if ($page == "peminjaman") {
              include "page/peminjaman/peminjaman.php";
            }else{
            include "./DashboardFitur.php";
            $dashboard=new DashboardFitur();
            ?>
            <div class="row">
              <div class="section pl-0  pe-0 pt-0 col m12 s12 pb-18">
                  <!--card stats start-->
                  <div id="card-stats">
                      <div class="row">
                          <div class="col s12 m6 l4">
                              <div class="card">
                                  <div class="card-content  green white-text">
                                      <p class="card-stats-title"><i class="mdi-social-people"></i>User Aktif</p>
                                      <h4 class="card-stats-number"><?=$dashboard->ambil_user_aktif();?> User</h4>
                                      <p class="card-stats-compare"><i class="mdi-hardware-keyboard-arrow-up"></i> <?=($dashboard->ambil_user_aktif() / $dashboard->ambil_semua_user() * 100);?>% <span class="green-text text-lighten-5">User Inventaris yang aktif</span>
                                      </p>
                                  </div>
                                  <div class="card-action  green darken-2">
                                      <div id="clients-bar" class="center-align"></div>
                                  </div>
                              </div>
                          </div>
                          <div class="col s12 m6 l4">
                              <div class="card">
                                  <div class="card-content pink lighten-1 white-text">
                                      <p class="card-stats-title"><i class="mdi-action-list"></i>Total Peminjaman Hari Ini </p>
                                      <h4 class="card-stats-number"><?=$dashboard->ambil_jumlah_peminjaman();?></h4>
                                      <p class="card-stats-compare"><i class="mdi-hardware-keyboard-arrow-down"></i><?=round($dashboard->ambil_jumlah_peminjaman()/$dashboard->ambil_data_inventaris()*100,2);?>%<span class="deep-purple-text text-lighten-5">Data Hari ini</span>
                                      </p>
                                  </div>
                                  <div class="card-action  pink darken-2">
                                      <div id="invoice-line" class="center-align"></div>
                                  </div>
                              </div>
                          </div>
                          <div class="col s12 m6 l4">
                              <div class="card">
                                  <div class="card-content purple white-text">
                                      <p class="card-stats-title"><i class="mdi-action-view-quilt "></i>Total Inventaris</p>
                                      <h4 class="card-stats-number"><?=$dashboard->ambil_semua_inventaris();?></h4>
                                      <p class="card-stats-compare"><i class="mdi-hardware-keyboard-arrow-up"></i>
                                      </p>
                                  </div>
                                  <div class="card-action purple darken-2">
                                      <div id="sales-compositebar" class="center-align"></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!--card stats end-->
              <div class="section card pt-0 col m12 s12 pb-18">
                <h4 class="card-header">Peminjaman</h4>
                <div class="container mt-10 mb-10">
                  <input type="text" name="NamaBarang" class="form-control custom-control" placeholder="Cari Berdasarkan Nama Barang atau ID" id="kode" autofocus=""> 
                  <table class=" display bordered custom-table" cellspacing="1">
                    <thead>
                      <tr>
                        <th class="wd-44 center">No</th>
                        <th>Nama Inventaris</th>
                        <th>Stok</th>
                        <th>Jumlah</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="showData">
                    </tbody>
                    <tbody id="Datanotfound">
                      <tr>
                        <td colspan="5" class="center">Tabel Kosong</td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="form-group" id="showBtnPinjam">
                    <button class="btn btn-custom right" id="btn-submit">Pinjam</button>
                  </div>
                </div>
              </div>
              <div class="section card pt-0 col m4 s12">
                  <!-- <h4 class="card-header">Total Peminjam Hari Ini</h4>
                <div class="container mt-10">
                <div class="col s12 m4 l2">
                            <p class="z-depth-5 shadow-demo"></p>
                          </div>
                </div> -->
              </div>
            </div>
            <?php
          }
          ?>
          <!-- Floating Action Button -->
          <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
          <a id="help" onclick="OpenModal('Help','help.php')" class="btn-floating btn-large"><i class="mdi-communication-live-help"></i></a>
         </div>
         <!-- Floating Action Button -->
       </div>
       <!--end container-->
     </section>
     <!-- END CONTENT -->

   </div>
   <!-- END WRAPPER -->

 </div>

 <script type="text/javascript" src="js/plugins/prism/prism.js"></script>
 <!--scrollbar-->
 <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="js/export/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="js/export/dataTables.buttons.min.js"></script>
 <script type="text/javascript" src="js/plugins.min.js"></script>
 <script type="text/javascript" src="js/jquery-ui.min.js"></script>
 <script type="text/javascript" src="js/autocomplete.js"></script>
 <script type="text/javascript" src="js/proses_pinjam.js"> </script> 
</body>
<?php
include'footer.php';
echo "</html>";
}
}else
{
  header('location:index.php');
}
?>