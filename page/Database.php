<?php
// Mulai Session
session_start();
// Bagian Pemanggilan Component
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
// Panggil Configurasi
include "config/config.php";
// Class Database Trurunan Dari Class Config yang di panggil di atas
class Database extends Config{
	// inisialisasi variable mysql
	private $mysqli;
	// function yang pertama kali di jalankan
	function __construct(){
		$this->mysqli= new mysqli($this->host,$this->username,$this->pass,$this->db); //Koneksi Ke database host,username,password di ambil dari class config
		if ($this->mysqli->connect_errno==1049) {
			header("location:restore.php");
		}/* else{
			echo "Koneksi Tidak Terhubung";
		} */
	}
	// Ini fungsi getlist
	public function get_list($table){ //parameter nama table yang akan di getlist
		$query = "SELECT * FROM ".$table." WHERE terhapus=0 ORDER BY id_".$table." DESC";
		$hasil = $this->mysqli->prepare($query);
		$hasil->execute();
		$hasil->store_result();
		return $hasil;
	}
	// Ini getlist dengan join table
	public function get_list_with_join($table, $type_join, $relation){ //parameter1 diisi dengan nama table, parameter 2 type JOIN yang akan dipakai, misal LEFT JOIN, INNER JOIN, parameter 3 table yang akan di relasikan jika lebih dari satu dapat menggunakan koma(,) misal table1,table2
		$join=explode(',',$relation); // convert string jadi array dengan exploid dengan delimeter koma(,)
		$query = "SELECT * FROM ".$table;

		foreach ($join as $value) {
			$query = $query." ".$type_join." ".$value." ON ".$table.".id_".$value."=".$value.".id_".$value;
		}

		$query = $query." WHERE ".$table.".terhapus=0 ORDER BY ".$table.".id_".$table." DESC";
		$hasil = $this->mysqli->query($query);
		return $hasil;
	}
	// Ini buat Get By Id
	public function get_by_id($table,$id){ // parameter 1 nama tablenya, parameter ke dua id yang akan di get
		$query = "SELECT * FROM ".$table." WHERE id_".$table."=".$id." AND terhapus=0";
		$hasil = $this->mysqli->prepare($query);
		$hasil->execute();
		$hasil->store_result();
		return $hasil;
	} 
	// Ini Buat get Where
	public function get_where($table,$field,$isi){ // parameter 1 nama tablenya, param ke dua field yang akan di filter, param ke 3 isinya
		$query = "SELECT * FROM ".$table." WHERE ".$field."='".$isi."' AND terhapus=0";
		$hasil = $this->mysqli->query($query);
		return $hasil;
	}
	// ini buat fungsi update
	public function update($table,$field,$isi,$id){ // parameter 1 nama tablenya, parameter ke 2 field yang akan di where, parameter ke tiga data yang akan di ubah, parameter ke 4 id yang akan di ubah
		$query = "UPDATE ".$table." SET ".$field."='".$isi."' WHERE id_".$table."='".$id."' AND terhapus=0";
		$hasil = $this->mysqli->query($query);
		return $hasil;
	}
	// Ini buat Query Manual
	public function query($query){ // parameter ke 1 query yang di panggilm, misal SELECT * FROM table_name
		$hasil = $this->mysqli->query($query);
		return $hasil;
	}
	// Ini Fungsi Untuk Kembalikan ke halaman sebelumnya
	public function back($msg=null,$mode=null){ //parameter ke 1 pesan yang akan di tampilkan defaultnya null, parameter ke dua mode 
		$_SESSION['message']=$msg?$msg:null; // Check variable message dari parameter ke 1
		if($mode !== null){
			if($mode=="reset"){
				echo"<script>window.location=window.location.origin+'/Sarpranas_EN2019/index.php'</script>";
			}else{
				echo"<script>window.location=window.location.origin+'/Sarpranas_EN2019/index.php?".$mode."'</script>";
			}
		}else{
			echo"<script>window.history.back();</script>";
		}
	}
	// Ini Fungsi buat nampilin loading
	public function loading(){
		echo'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="msapplication-tap-highlight" content="no">
		<title>Loading...</title>  
		<base href="'.$this->base_url.'">
		<link rel="icon" href="images/S.png" sizes="32x32">
		<link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
		<link href="css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
		<link href="css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">
		<div class="row" style="margin-top: 250px;">
		<div class="col s12 m12 center">
		<div id="loader"></div>        				
		<div>Tunggu</div>
		</div>
		</div>';
	}
	// Ini Fungsi Buat mendapatkan String acak
	public function randomString($api="",$leng=8) //
    {
        if (!$api) {
            $use = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; 
        }else{
            $use = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; 
        }
        srand((double)microtime()*1000000); 
        if ($api) {
            $api .= '-';
        }
        for($i=0; $i<$leng; $i++) { 
          $api .= $use[rand()%strlen($use)]; 
        } 
      return $api; 
	}
	// ini fungsi Untuk ngirim Email
	public function send_email($judul_email,$isi_email,$penerima){ //parameter ke 1 judul emailnya, parameter ke 2 isi email, parameter 3 penerima email
		require $_SERVER['DOCUMENT_ROOT'].'/SARPRANAS_EN2019/vendor/autoload.php'; // panggil autoload untuk component 
	    $mail = new PHPMailer(); // create a new object
		$mail->IsSMTP(true); // enable SMTP
		$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
		$mail->SMTPAuth = true; // authentication enabled
		$mail->SMTPSecure = 'tls';
		$mail->Host = 'smtp.gmail.com';
		// $mail->Host = "system.sarpranas@gmail.com";
		$mail->Port = 587; // or 587
		$mail->IsHTML(true); 
		$mail->Username = "sarpranasEN19@gmail.com";
		$mail->Password = "r00tus3r";
		$mail->SetFrom("system.sarpranas@gmail.com");
		$mail->FromName="Sarpranas System";
		$mail->Subject = $judul_email;
		$mail->AddEmbeddedImage("images/SG.png", "my-attach","Sarpranas");
		$mail->Body = '<div style="margin:20px; width:400px; box-shadow:6px 5px 9px #4444;"><div style="padding:17px;color:#fff;background:#00bcd4;width:100%;"><img src="cid:my-attach" style="width:5%" alt="" /><span style="font-size: 1.3rem; font-weight: bold; ">arpranas System</span></div>'.$isi_email;
		$mail->AddAddress($penerima);

		 if(!$mail->Send()) {
	    	return 'gagal';
		 } else {
	    	return 'Berhasil';
		 }
	}
}