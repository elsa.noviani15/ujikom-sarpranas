<?php 
  include "../Database.php";
  $db=new Database();
  $db->loading();
  if(isset($_GET['mode']) && $_GET['mode']=='kembali-detail'){
    $id=$_GET['id'];
    $query_select=$db->get_where('detail_pinjam','id_detail_pinjam',$id);
    $tampil=mysqli_fetch_assoc($query_select);
    $query_update=$db->query("UPDATE inventaris SET jumlah=jumlah+$tampil[jumlah] WHERE id_inventaris=$tampil[id_inventaris]");
    if ($query_update) {
      $tanggal=DATE('Y-m-d H:m:s');
      $query_update_detail=$db->query("UPDATE detail_pinjam SET tanggal_kembali='$tanggal' WHERE id_detail_pinjam=$id");
      if($query_update_detail){
        $check=$db->query("SELECT * FROM detail_pinjam WHERE id_peminjaman=$tampil[id_peminjaman] AND tanggal_kembali is NULL");
        if(mysqli_num_rows($check) > 0){
          $db->back('Berhasil Mengembalikan Barang');
        }else{
          $query_update_peminjaman=$db->query("UPDATE peminjaman SET tanggal_kembali='$tanggal',status_peminjaman='Sudah Di Kembalikan' WHERE id_peminjaman='$tampil[id_peminjaman]'");
          if($query_update_peminjaman){
            $db->back('Berhasil Mengembalikan Barang');
          }else{
            $db->back('Berhasil Update pengembalian');
          }
        }
      }else{
        $db->back('Gagal Update details.');
      }
    }else{
      $db->back('Gagal Update Stok Inventaris.');
    }
}else if(isset($_GET['mode']) && $_GET['mode']=='kembali-semua'){
  $id=$_GET['id'];
  $query_select=$db->get_where('detail_pinjam','id_peminjaman',$id);
    while($tampil=mysqli_fetch_array($query_select)){
      $query_update=$db->query("UPDATE inventaris SET jumlah=jumlah+$tampil[jumlah] WHERE id_inventaris=$tampil[id_inventaris]");          
    }
    $tanggal=DATE('Y-m-d H:m:s');
    $query_update_detail=$db->query("UPDATE detail_pinjam SET tanggal_kembali='$tanggal' WHERE id_peminjaman=$id");
    $query_update_peminjaman=$db->query("UPDATE peminjaman SET tanggal_kembali='$tanggal',status_peminjaman='Sudah Di Kembalikan' WHERE id_peminjaman='$id'");
      if($query_update_peminjaman){
        $db->back('Berhasil Mengembalikan Barang');
      }else{
        $db->back('Berhasil Update pengembalian');
      }
}else{
  if(isset($_POST['id'])){
  //   // Ini Buat Edit
    $tanggal=date("Y-m-d");
    $status="Sedang Dipinjam";
    $query="UPDATE peminjaman SET id_inventaris='$_POST[id_inventaris]','$tanggal','$status'',id_member='$_POST[id_member]',id_jenis='$_POST[id_jenis]','$tanggal',id_ruang='$_POST[id_ruang]',kode_inventaris='$_POST[kode_inventaris]',id_petugas='$_POST[id_petugas]' WHERE id_inventaris=$_POST[id] AND terhapus=0";
    if($db->query($query)){
      $db->back('Berhasil Edit Data.');
    }else{
      $db->back('Gagal Edit Data.');
    }
  }else if(isset($_GET['id'])){
    // Ini soft Delete jadi di table terhapus di db mah tetep ada
    $query="UPDATE inventaris SET terhapus=1  WHERE id_inventaris=$_GET[id] AND terhapus=0";
    if($db->query($query)){
      $db->back('Berhasil Hapus Data.');
    }else{
      $db->back('Gagal Hapus Data.');
    }
  }else{
    //Proses Create Data
    $tanggal=date("Y-m-d");
    $query="INSERT INTO content (nama,kondisi,keterangan,jumlah,id_jenis,tanggal,id_ruang,kode_inventaris,id_petugas) VALUES ('$_POST[nama]','$_POST[kondisi]','$_POST[keterangan]','$_POST[jumlah]','$_POST[id_jenis]','$tanggal','$_POST[id_ruang]','$_POST[kode_inventaris]','$_POST[id_petugas]')";
    if($db->query($query)){
      $db->back('Berhasil Menambahkan Data.');
    }else{
      $db->back('Gagal Menambahkan Data.');
    }
  }
}
?>