<?php if(isset($_SESSION['message'])){
  echo"<script>document.getElementById('showToast').click();</script>";
  unset($_SESSION['message']);
}?>
<div class="section card pt-0">
  <h4 class="card-header">Peminjaman</h4>
  <div class="container mt-10">
    <!-- <button class="btn btn-custom" onclick="OpenModal('Peminjaman - Form','page/peminjaman/form.php');"><i class="large mdi-content-add"></i> <span>Tambah</span></button> -->
    <div class="table-responsive" id="table-datatables">
      <table id="data-table-simple" class="display bordered" cellspacing="0">
        <thead>
          <tr>
            <th class="wd-44 center">No</th>
            <th>Nama Peminjam</th>
            <th>Tanggal Peminjaman</th>
            <th>Tanggal Pengembalian</th>
            <th>Status</th>
            <th class="center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          $db= new Database();
          $select = $db->get_list_with_join('peminjaman','INNER JOIN','petugas');
          foreach ($select as $show)
          {
            ?>
            <tr>
             <td class="center"><?= $no++; ?></td>
             <td><?= $show['nama_petugas']; ?></td>
             <td><?= $show['tanggal_pinjam']; ?></td>
             <td><?= $show['tanggal_kembali']; ?></td>
             <td><?= $show['status_peminjaman']; ?></td>
             <td class="center"> 
               <a href="#" onclick="OpenModal('Detail Peminjam','page/peminjaman/form.php?id=<?=$show['id_peminjaman'];?>')" title="Details"><i class="mdi-action-visibility blue-text"></i></a>
               <?php if($show['tanggal_kembali'] == '0000-00-00 00:00:00'){ ?>
                  <a href="page/peminjaman/proses.php?mode=kembali-semua&id=<?=$show['id_peminjaman'];?>" title="Kembalikan"><i class="mdi-content-undo blue-text"></i></a>
                <?php } ?>
               <!-- <a href="#" onclick="OpenModal('Peminjaman - Form','page/peminjaman/form.php?id=<?=$show['id_peminjaman'];?>')"><i class="mdi-content-undo blue-text"></i></a> -->
               <!-- <a href="#" onclick="showDialogDelete('page/Peminjaman/proses.php?id=<?=$show['id_peminjaman'];?>')"><i class="mdi-action-delete red-text"></i></a> -->
             </td>
           </tr>
           <?php
         }
         ?>
       </tbody>
     </table>
   </div>
 </div>