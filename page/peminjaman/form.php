<?php
include "../Database.php";
$id=isset($_GET['id'])?$_GET['id']:null;
$db=new Database();
if(isset($id)){
  $select=$db->query("SELECT peminjaman.*,petugas.nama_petugas FROM peminjaman JOIN petugas ON peminjaman.id_petugas=petugas.id_petugas WHERE peminjaman.id_peminjaman='$id'");
  $show=mysqli_fetch_assoc($select);
}
?>
<form id="formModal" method="POST" action="" class="col s12">
  <div class="row">
    <div class="input-field col s6">
      <?=isset($id)?'<input type="hidden" name="id_peminjaman" value="'.$id.'">':'';?>
      <input type="text" name="status" class="validate" required="" value="<?=isset($show['nama_petugas'])?$show['nama_petugas']:'';?>" readonly>
      <label for="id_inventaris" class="<?=isset($id)?'active':'';?>">Nama Peminjam</label>
    </div>
    <div class="input-field col s6">
      <input type="text" name="tanggal_peminjaman" class="validate" required="" value="<?=isset($show['tanggal_pinjam'])?$show['tanggal_pinjam']:'';?>" readonly>
      <label for="tanggal_pinjam" class="<?=isset($id)?'active':'';?>">Tanggal Pinjam</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s6">
      <input type="text" name="status" class="validate" required="" value="<?=isset($show['status_peminjaman'])?$show['status_peminjaman']:'';?>" readonly>
      <label for="id_inventaris" class="<?=isset($id)?'active':'';?>">Status</label>
    </div>
    <div class="input-field col s6">
      <input type="text" name="tanggal_kembali" class="validate" required="" value="<?=isset($show['tanggal_kembali'])?$show['tanggal_kembali']:'';?>" readonly>
      <label for="tanggal_kembali" class="<?=isset($id)?'active':'';?>">Tanggal Pengembalian</label>
    </div>
  </div>
  <div class="row">
  <b><u>Details</u></b>
  <div class="input-field col s12">
    <div class="table-responsive" id="table-datatables">
        <table id="data-table-simple" class="display bordered custom-table" cellspacing="0">
          <thead>
            <tr>
              <th class="wd-44 center">No</th>
              <th>Nama Barang</th>
              <th>Jumlah</th>
              <th class="center">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no=1;
              $q=$db->query("SELECT d.*,i.nama as nama_barang FROM detail_pinjam d INNER JOIN inventaris i ON i.id_inventaris=d.id_inventaris WHERE d.id_peminjaman='$show[id_peminjaman]'");
              while($s=mysqli_fetch_array($q)){
            ?>
            <tr>
                <td class="center"><?=$no++;?></td>
                <td><?=$s['nama_barang'];?></td>
                <td><?=$s['jumlah'];?></td>
                <td>
                  <?php 
                  if($s['tanggal_kembali'] == NULL){
                    echo"<a href='page/peminjaman/proses.php?mode=kembali-detail&id=$s[id_detail_pinjam];'>Kembalikan</a>";
                  }else{
                    echo "Sudah di kembalikan pada tanggal : $s[tanggal_kembali]";
                  }
                  ?>
                </td>
            </tr>
            <?php 
              }
              ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
  <div class="row">
    <div class="col m12">
      <button type="button" href="#" class="btn red right" onclick="CloseModal()">Cancel</button>
    </div>
  </div>
<style>
.modal{
  width:75%!important;
}</style>
</form>