<?php
  include "../Database.php";
  $id=isset($_GET['id'])?$_GET['id']:null;
  $db=new Database();
  if(isset($id)){
    $select=$db->get_by_id('detail_pinjam',$id);
    $select->bind_result($id, $id_inventaris, $id_peminjaman,$jumlah,$terhapus);
    $select->fetch();
  }
  ?>
<form id="formModal" method="POST" action="./page/detail_pinjam/proses.php" class="col s12">
  <div class="row">
    <div class="input-field col s12">
      <?=isset($id)?'<input type="hidden" name="id" value="'.$id.'">':'';?>
      <input type="number" name="id_inventaris" class="validate" required="" value="<?=isset($id_inventaris)?$id_inventaris:'';?>" autofocus>
      <label for="id_inventaris" class="<?=isset($id_inventaris)?'active':'';?>">No Inventaris</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="number" name="id_peminjaman" length="50" class="validate" value="<?=isset($id_peminjaman)?$id_peminjaman:'';?>" required="">
      <label for="id_peminjaman" class="<?=isset($id_peminjaman)?'active':'';?>">No Peminjam</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="number" name="jumlah" length="50" class="validate" value="<?=isset($jumlah)?$jumlah:'';?>" required="">
      <label for="jumlah" class="<?=isset($jumlah)?'active':'';?>">Jumlah</label>
    </div>
  </div>
  <div class="row">
  	<div class="col m12">
  		<input type="submit" name="submit" value="Simpan" class="btn blue right ml-10">
  		<button type="button" href="#" class="btn red right" onclick="CloseModal()">Cancel</button>
  	</div>
  </div>
</form>