<?php if(isset($_SESSION['message'])){
  echo"<script>document.getElementById('showToast').click();</script>";
  unset($_SESSION['message']);
}?>
<div class="section card pt-0">
  <h4 class="card-header">Detail Peminjaman</h4>
  <div class="container mt-10">
    <button class="btn btn-custom" onclick="OpenModal('Detail Peminjaman - Form','page/detail_pinjam/form.php');"><i class="large mdi-content-add"></i> <span>Tambah</span></button>
    <div class="table-responsive" id="table-datatables">
      <table id="data-table-simple" class=" display bordered" cellspacing="0">
        <thead>
          <tr>
            <th class="wd-44 center">No</th>
            <th>Nama Inventaris</th>
            <th>Nama Peminjam</th>
            <th>Jumlah</th>
            <th class="center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          $db= new Database();
          $select = $db->get_list_with_join('detail_pinjam','INNER JOIN','inventaris,peminjaman');
          {
            ?>
            <tr>
             <td class="center"><?= $no++; ?></td>
             <td><?= $id_inventaris; ?></td>
             <td><?= $id_peminjaman; ?></td>
             <td><?= $jumlah; ?></td>
             <td class="center"> 
               <a href="#" onclick="OpenModal('Detail Peminjaman - Form','page/detail_pinjam/form.php?id=<?=$id;?>')"><i class="mdi-image-edit blue-text"></i></a>
               <a href="#" onclick="showDialogDelete('page/detail_pinjam/proses.php?id=<?=$id;?>')"><i class="mdi-action-delete red-text"></i></a>
             </td>
           </tr>
           <?php
         }
         ?>
       </tbody>
     </table>
     <button class="btn btn-custom"><i class="large mdi-action-backup"></i> <span>Backup Data</span></button>
   </div>
 </div>