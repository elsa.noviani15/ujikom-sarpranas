<?php if(isset($_SESSION['message'])){
  echo"<script>document.getElementById('showToast').click();</script>";
  unset($_SESSION['message']);
}?>
<div class="section card pt-0">
  <h4 class="card-header">User</h4>
  <div class="container mt-10" >
    <button class="btn btn-custom" onclick="OpenModal('User - Form','page/user/form.php');"><i class="large mdi-content-add"></i> <span>Tambah</span></button>
    <div class="table-responsive" id="table-datatables">
      <table id="data-table-simple" class="display bordered" cellspacing="0">
        <thead>
          <tr>
            <th class="wd-44 center">No</th>
            <th>Username</th>
            <th>Nama Petugas</th>
            <th>E-Mail</th>
            <th>Level</th>
            <th class="center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          $db= new Database();
          $select = $db->get_list_with_join('petugas','INNER JOIN','level');
          foreach ($select as $show)
          {
            ?>
            <tr>
              <td class="center"><?= $no++; ?></td>
              <td><?= $show['username']; ?></td>
              <td><?= $show['nama_petugas']; ?></td>
              <td><?= $show['email']; ?></td>
              <td><?= $show['nama_level']; ?></td>
              <td class="center"> 
                <a href="#" onclick="OpenModal('User - Form','page/user/form.php?id=<?=$show[id_petugas];?>')"><i class="mdi-image-edit blue-text"></i></a>
                <a href="#" onclick="showDialogDelete('page/user/proses.php?id=<?=$show[id_petugas];?>')"><i class="mdi-action-delete red-text"></i></a>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
   </div>
   </div>
 </div>