c<?php
include "../Database.php";
$id_petugas=isset($_GET['id'])?$_GET['id']:null;
$db=new Database();
if(isset($id_petugas)){
  $select=$db->get_by_id('petugas',$id_petugas);
  $select->bind_result($id_petugas, $username, $password, $nama_petugas, $email,$token,$id_level,$aktif,$terhapus);
  $select->fetch();
}
?>
<form id="formModal" method="POST" action="./page/user/proses.php" class="col s12">
  <div class="row">
    <div class="input-field col s12">
      <?=isset($id_petugas)?'<input type="hidden" name="id" value="'.$id_petugas.'">':'';?>
      <input type="text" name="username" class="validate" required="" value="<?=isset($username)?$username:'';?>" autofocus maxlength="50">
      <label for="username" class="<?=isset($username)?'active':'';?>">Username</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="password" name="password" class="validate" length="120" value="<?=isset($password)?$password:'';?>" required=""  maxlength="20">
      <label for="password" class="<?=isset($password)?'active':'';?>">Password</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="text" name="nama_petugas" class="validate" length="120" value="<?=isset($nama_petugas)?$nama_petugas:'';?>" required=""  maxlength="100">
      <label for="nama_petugas" class="<?=isset($nama_petugas)?'active':'';?>">Nama Petugas</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="text" name="email" class="validate" value="<?=isset($email)?$email:'';?>" required=""  length="3">
      <label for="email" class="<?=isset($email)?'active':'';?>">E-Mail</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <select name="id_level">
        <option value="" hidden="">Pilih Level</option>
        <?php
        $select = $db->get_list('level');
        $select->bind_result($id, $nama_level,$terhapus);
        while($select->fetch())
          {?>
            <option value="<?=$id;?>" <?=(($id_petugas != null) && ($id_level==$id))?'selected':'';?>><?=$nama_level;?></option>
            <?php
          }
          ?>
        </select>
        <label>Level</label>
      </div>
    </div>
    <div class="row">
      <div class="col m12">
        <input type="submit" name="submit" value="Simpan" class="btn blue right ml-10">
        <button type="button" href="#" class="btn red right" onclick="CloseModal()">Cancel</button>
      </div>
    </div>
  </form>