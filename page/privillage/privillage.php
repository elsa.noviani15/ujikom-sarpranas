<div class="table-responsive" id="table-datatables">
  <div class="container">
    <h4 class="header">Privillage</h4>
    <a class="btn-floating blue darken-1" onclick="OpenModal('create','Tambah Privillage')"><i class="large mdi-content-add"></i></a>
    
    <table id="data-table-simple" class=" display" cellspacing="0">
      <thead>
        <tr>
          <th>No</th>
          <th>Username</th>
          <th>Password</th>
          <th>Nama Petugas</th>
          <th>E-mail</th>
          <th>Token</th>
          <th>Level</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
       <?php
       $e = 1;
       $select = $koneksi->prepare("SELECT id_petugas, username, password, nama_petugas,email,token,id_level FROM petugas");
       $select->execute();
       $select->store_result();
       $select->bind_result($id_petugas, $username, $password,$nama_petugas,$email,$token,$id_level);
       while($select->fetch())
       {
         ?>
         <tr>
          <td><?= $e++; ?></td>
          <td><?= $username; ?></td>
          <td><?= $password; ?></td>
          <td><?= $nama_petugas; ?></td>
          <td><?= $email; ?></td>
          <td><?= $token; ?></td>
          <td><?= $id_level; ?></td>
          <td> 
            <a class="btn-floating blue darken-1" href="?page=privillage&action=edit&id_petugas=<?=$id_petugas;?>"><i class="mdi-image-edit"></i></a>
            <a class="btn-floating red darken-1" onclick="return confirm('Yakin hapus data?')" href="?page=privillage&action=hapus&id_petugas=<?=$id_petugas;?>"><i class="mdi-action-delete"></i></a>
          </td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
</div>
</div>