<?php if(isset($_SESSION['message'])){
  echo"<script>document.getElementById('showToast').click();</script>";
  unset($_SESSION['message']);
}?>
<div class="section card pt-0">
  <h4 class="card-header">Inventaris</h4>
  <div class="container mt-10" >
    <button class="btn btn-custom" onclick="OpenModal('Inventaris - Form','page/inventaris/form.php');"><i class="large mdi-content-add"></i> <span>Tambah</span></button>
   <div  id="getReport">
    <div class="table-responsive" id="table-datatables getReport">
      <table id="data-table-simple" class="table table-bordered table-striped table-hover js-basic-example dataTable custom-table display" cellspacing="0">
        <thead>
          <tr>
            <th class="wd-44 center">No</th>
            <th>Nama Barang</th>
            <th>Kondisi</th>
            <th>Jumlah</th>
            <th>Jenis</th>
            <th>Tanggal Registrasi</th>
            <th>Ruang</th>
            <th>Kode Inventaris</th>
            <th>Petugas</th>
            <th>Keterangan</th>
            <?php if(empty($_GET['report'])){?>
            <th class="center">Action</th>
            <?php }?>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          $db= new Database();
          $select = $db->get_list_with_join('inventaris','INNER JOIN','jenis,ruang,petugas');
          foreach ($select as $show)
          // INi buat report
          // Tutup
          {
                if(isset($_GET['report'])){
                  if($show['id_jenis']==$_GET['id-jenis']){
                    ?>
                      <tr>
                        <td class="center"><?= $no++; ?></td>
                        <td><?= $show['nama']; ?></td>
                        <td><?= $show['kondisi']; ?></td>
                        <td><span class="right"><?= $show['jumlah']; ?></span></td>
                        <td><?= $show['nama_jenis']; ?></td>
                        <td><?= $show['tanggal_register']; ?></td>
                        <td><?= $show['nama_ruang']; ?></td>
                        <td><?= $show['kode_inventaris']; ?></td>
                        <td><?= $show['nama_petugas']; ?></td>
                        <td title="<?=$show['keterangan'];?>"><?=$show['keterangan']; ?></td>
                      </tr>
                    <?php
                  }
                }else{
            ?>
            <tr>
              <td class="center"><?= $no++; ?></td>
              <td><?= $show['nama']; ?></td>
              <td><?= $show['kondisi']; ?></td>
              <td><span class="right"><?= $show['jumlah']; ?></span></td>
              <td><?= $show['nama_jenis']; ?></td>
              <td><?= $show['tanggal_register']; ?></td>
              <td><?= $show['nama_ruang']; ?></td>
              <td><?= $show['kode_inventaris']; ?></td>
              <td><?= $show['nama_petugas']; ?></td>
              <td title="<?=$show['keterangan'];?>"><?= substr($show['keterangan'],0,8); ?></td>
              <?php if(empty($_GET['report'])){?>
              <td class="center"> 
                <a href="#" onclick="OpenModal('Inventaris - Form','page/inventaris/form.php?id=<?=$show[id_inventaris];?>')"><i class="mdi-image-edit blue-text"></i></a>
                 <a href="#" onclick="showDialogDelete('page/inventaris/proses.php?id=<?=$show[id_inventaris];?>')"><i class="mdi-action-delete red-text"></i></a>
              </td>
              <?php }?>
            </tr>
            <?php
          }
          }
          ?>
        </tbody>
      </table>
   </div>
   </div>
 </div>