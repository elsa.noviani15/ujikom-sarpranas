c<?php
include "../Database.php";
$id_inventarisis=isset($_GET['id'])?$_GET['id']:null;
$db=new Database();
if(isset($id_inventarisis)){
  $select=$db->get_by_id('inventaris',$id_inventarisis);
  $select->bind_result($id_inventaris, $nama, $kondisi, $keterangan, $jumlah,$id_jenis,$tanggal_register,$id_ruang,$kode_inventaris,$id_petugas,$terhapus);
  $select->fetch();
}
?>
<form id="formModal" method="POST" action="./page/inventaris/proses.php" class="col s12">
  <div class="row">
    <div class="input-field col s12">
      <?=isset($id_inventaris)?'<input type="hidden" name="id" value="'.$id_inventaris.'">':'';?>
      <input type="text" name="nama" class="validate" required="" value="<?=isset($nama)?$nama:'';?>" autofocus maxlength="50">
      <label for="nama" class="<?=isset($nama)?'active':'';?>">Nama Inventaris</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="text" name="kondisi" class="validate" length="120" value="<?=isset($kondisi)?$kondisi:'';?>" required=""  maxlength="20">
      <label for="kondisi" class="<?=isset($kondisi)?'active':'';?>">Kondisi</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="text" name="keterangan" class="validate" length="120" value="<?=isset($keterangan)?$keterangan:'';?>" required=""  maxlength="100">
      <label for="keterangan" class="<?=isset($keterangan)?'active':'';?>">Keterangan</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="number" name="jumlah" class="validate" value="<?=isset($jumlah)?$jumlah:'';?>" required=""  length="3">
      <label for="jumlah" class="<?=isset($jumlah)?'active':'';?>">Jumlah</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <select name="id_jenis">
        <option value="" hidden="">Pilih Jenis Inventaris</option>
        <?php
        $select = $db->get_list('jenis');
        $select->bind_result($id, $nama_jenis, $kode_jenis,$keterangan,$terhapus);
        while($select->fetch())
          {?>
          <?=$nama_jenis;?>
            <option value="<?=$id;?>" <?=(($id_inventarisis != null) && ($id_jenis==$id))?'selected':'';?> ><?=$nama_jenis;?></option>
          <?php
          }
          ?>
        </select>
        <label>Jenis</label>
      </div>
    </div>
  <div class="row">
    <div class="input-field col s12">
      <select name="id_ruang">
        <option value="" hidden="">Pilih Ruang Inventaris</option>
        <?php
        $select = $db->get_list('ruang');
        $select->bind_result($id, $nama_ruang, $kode_ruang,$keterangan,$terhapus);
        while($select->fetch())
          {?>
            <option value="<?=$id;?>" <?=(($id_inventarisis != null) && ($id_ruang==$id))?'selected':'';?>><?=$nama_ruang;?></option>
            <?php
          }
          ?>
        </select>
        <label>Ruang</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12">
        <input type="number" name="kode_inventaris" class="validate" length="120" value="<?=isset($kode_inventaris)?$kode_inventaris:'';?>" required="">
        <label for="kode_inventaris" class="<?=isset($kode_inventaris)?'active':'';?>">Kode Inventaris</label>
      </div>
    </div>
<!--     <div class="row">
      <div class="input-field col s12">
        <input type="number" name="id_petugas" class="validate" length="120" value="<?=isset($id_petugas)?$id_petugas:'';?>" required="">
        <label for="id_petugas" class="<?=isset($id_petugas)?'active':'';?>">Petugas</label>
      </div>
    </div> -->
    <div class="row">
      <div class="col m12">
        <input type="submit" name="submit" value="Simpan" class="btn blue right ml-10">
        <button type="button" href="#" class="btn red right" onclick="CloseModal()">Cancel</button>
      </div>
    </div>
  </form>