<div class="card">
  <h4 class="card-header">Report Sarpranas</h4>
  <div class="container mt-10" style="min-height:513px;">
   <form action="" method="POST" class="col s12">
    <div class="row">
        <div class="input-field col m4 s12">
        <select name="nama_jenis" required="" id="KIB">
           <option value="" disabled selected>Pilih Nama Alat</option>
           <?php
            $db= new Database();
            $select = $db->get_list('jenis');
            $select->bind_result($id, $nama_jenis, $kode_jenis,$keterangan,$terhapus);
            while($select->fetch())
            {
           ?>
              <option value="<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>/inventaris&report=inventaris&id-jenis=<?=$id;?>" ><?=$nama_jenis;?></option>
            <?php 
            }
            ?>
              <option value="<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>/report-peminjaman" >Peminjaman</option>
            </select>
            <label>Jenis KIB</label>
        </div>
        <!-- <div class="input-field col s4 ShowDate">
            <label for="">Dari Tanggal</label>
            <input type="date" class="datepicker" name="dariTanggal" id="dariTanggal">
        </div>
        <div class="input-field col s4 ShowDate">
            <label for="">Sampai Tanggal</label>            
            <input type="date" class="datepicker" name="sampaiTanggal" id="sampaiTanggal">
        </div> -->
        <div class="col s12">
            <!-- <button class="btn btn-custom ShowDate" title="Silahkan Isi Dari Tanggal Sampai Tanggal di atas" type="button" onclick="tampilPeminjaman()">Lihat</button> -->
            <button class="btn btn-custom ShowDate" type="button" onclick="tampilPeminjaman('semua')">Lihat Semua</button>
            <button class="btn btn-custom" id="button-print" type="button" onClick="printTable()">
              <i class="mdi-file-file-download"></i><span> Export</span>
            </button>
        </div>
      </div>
   </form>
  <div class="row mt-10">
    <div class="col s12">
      <!-- Ini Report Inventaris  -->
        <div class="table-responsive showAfterChange" id="print">
          <style type="text/css">
            @import url(css/style.css) print;
          </style>
          <div id="loadContent"></div>
        </div>
        <!-- Ini Report Peminjaman -->
        <div class="table-responsive tampilReportPinjam" id="print" style="display:none">
          <style type="text/css">
            @import url(css/style.css) print;
          </style>
          <div id="loadContent1">
            <table class="table table-bordered table-striped table-hover js-basic-example dataTable custom-table custom-table-print display" cellspacing="0">
              <thead>
                <tr>
                  <th class="wd-44 center">No</th>
                  <th>Nama Peminjam</th>
                  <th>Tanggal Peminjaman</th>
                  <th>Tanggal Pengembalian</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 1;
                $db= new Database();
                $select = $db->get_list_with_join('peminjaman','INNER JOIN','petugas');
                foreach ($select as $show)
                {
                ?>
                  <tr>
                    <td class="center"><?=$no++; ?></td>
                    <td><?= $show['nama_petugas']; ?></td>
                    <td><?= $show['tanggal_pinjam']; ?></td>
                    <td><?= $show['tanggal_kembali']; ?></td>
                    <td><?= $show['status_peminjaman']; ?></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td colspan="4" style="padding:0!important;">
                      <table class="display bordered custom-table custom-table-print" cellspacing="0">
                        <thead>
                          <tr>
                            <th class="wd-44 center">No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th class="center">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                            $noo=1;
                              $q=$db->query("SELECT d.*,i.nama as nama_barang FROM detail_pinjam d INNER JOIN inventaris i ON i.id_inventaris=d.id_inventaris WHERE d.id_peminjaman='$show[id_peminjaman]'");
                              while($s=mysqli_fetch_array($q)){
                            ?>
                            <tr>
                                <td class="center"><?=$noo++;?></td>
                                <td><?=$s['nama_barang'];?></td>
                                <td><?=$s['jumlah'];?></td>
                                <td>
                                  <?php 
                                  if($s['tanggal_kembali'] == NULL){
                                    echo"Belum Di Kembalikan";
                                  }else{
                                    echo "Sudah di kembalikan pada tanggal : $s[tanggal_kembali]";
                                  }
                                  ?>
                                </td>
                            </tr>
                            <?php 
                              }
                              ?>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                <?php
               }
                ?>
            </tbody>
            </table>
            </div>
        </div>
      </div>
    </div>
  </div>
  <iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>
  <script>
    $('#button-print').hide();
    $(function(){
      $("#KIB").on("change",function(event){
        $('.ShowDate').hide();
        $('.tampilReportPinjam').hide();        
        $(".showAfterChange").show('fade');
        var url=event.currentTarget.value.split("page=");
        if(url[1].split('/')[1] == "report-peminjaman"){
          $('.ShowDate').show();
          $('#button-print').hide();
          $('#loadContent').hide();

        }else{
          url=url[0]+"page="+url[1].split("/")[1];
          $('#loadContent').show();
          $("#loadContent").load( url+ " " + "#getReport", function (response, status) {
          });
          $('#button-print').show();
        }
      });
    });
    function tampilPeminjaman(params) {
      if(params=="semua"){
        var semuaReportPinjam=true;
      }else{
        var semuaReportPinjam=false;        
      }
      $('.tampilReportPinjam').show();
      $('#button-print').show();        
    }
    function printTable() {
      window.frames["print_frame"].document.body.innerHTML="";
      if(window.frames["print_frame"].document.body.innerHTML==""){
        window.frames["print_frame"].document.write('<html><head><link rel="stylesheet" type="text/css" href="css/style.css" /></head><body>' + ($('.tampilReportPinjam')[0].style.display=="none"?(document.getElementById("loadContent").innerHTML):  (document.getElementById("loadContent1").innerHTML)) + '</html>');
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();
          }
       }
  </script>
 </div>
<style>
  .ShowDate{
    display:none;
  }
</style>