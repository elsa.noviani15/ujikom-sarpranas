DROP TABLE content;

CREATE TABLE `content` (
  `id_content` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `tanggal` date NOT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_content`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO content VALUES("1","SMKN 1 CIOMAS","","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\n  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n","2019-02-04","0");
INSERT INTO content VALUES("2","hsah","DSBHSA","SDHAB","0000-00-00","0");
INSERT INTO content VALUES("3","dsjs","asdnj","sjdna","0000-00-00","0");
INSERT INTO content VALUES("4","sac","sca","sa","0000-00-00","0");
INSERT INTO content VALUES("5","sdf","dsf","sdf","0000-00-00","1");
INSERT INTO content VALUES("6","asdadsa","asdas","asdas","0000-00-00","0");
INSERT INTO content VALUES("7","asd","asdas","asdas","0000-00-00","1");
INSERT INTO content VALUES("8","asdas","asdas","asdasd","2019-02-19","0");



DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `jumlah` int(30) NOT NULL,
  `tanggal_kembali` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_detail_pinjam`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("18","13","15","1","2019-03-19 10:03:33","0");
INSERT INTO detail_pinjam VALUES("19","14","15","31","2019-03-19 10:03:33","0");
INSERT INTO detail_pinjam VALUES("20","13","16","1","2019-03-18 13:03:04","0");
INSERT INTO detail_pinjam VALUES("21","14","16","1","2019-03-18 13:03:04","0");
INSERT INTO detail_pinjam VALUES("22","13","17","1","2019-03-18 13:03:13","0");
INSERT INTO detail_pinjam VALUES("23","14","18","1","2019-03-19 10:03:26","0");
INSERT INTO detail_pinjam VALUES("24","14","19","26","2019-03-19 10:03:29","0");
INSERT INTO detail_pinjam VALUES("25","13","19","1","2019-03-19 10:03:29","0");
INSERT INTO detail_pinjam VALUES("26","14","20","2","2019-03-19 10:03:35","0");
INSERT INTO detail_pinjam VALUES("27","14","21","3","2019-03-19 10:03:31","0");
INSERT INTO detail_pinjam VALUES("28","14","22","30","2019-03-19 10:03:03","0");
INSERT INTO detail_pinjam VALUES("29","14","23","4","2019-04-01 08:04:55","0");
INSERT INTO detail_pinjam VALUES("30","14","24","1","","0");
INSERT INTO detail_pinjam VALUES("31","14","25","1","2019-03-31 08:03:52","0");
INSERT INTO detail_pinjam VALUES("32","14","26","4","","0");
INSERT INTO detail_pinjam VALUES("33","17","26","5","","0");
INSERT INTO detail_pinjam VALUES("34","14","27","1","2019-04-08 15:04:53","0");
INSERT INTO detail_pinjam VALUES("35","14","28","1","2019-04-08 15:04:59","0");
INSERT INTO detail_pinjam VALUES("36","13","29","1","2019-04-08 15:04:06","0");
INSERT INTO detail_pinjam VALUES("37","14","30","20","","0");
INSERT INTO detail_pinjam VALUES("38","14","31","1","","0");
INSERT INTO detail_pinjam VALUES("39","13","32","4","","0");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `kondisi` varchar(50) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("13","Proyektor","Baru","Alat ini digunakan untuk menampilkan informasi pada saat presentasi agar penyampaiannya menarik","7","6","2019-03-18","5","1001","1","0");
INSERT INTO inventaris VALUES("14","Meja Lab RPL","Baru","Barang ini milik jurusan RPL berdasarkan dana bantuan BOS untuk keperluan KBM RPL","44","6","2019-03-18","4","1002","1","0");
INSERT INTO inventaris VALUES("15","Cable VGA","Baru","Alat ini digunakan untuk menyambungkan proyektor dengan laptop","40","6","2019-03-19","5","1003","1","0");
INSERT INTO inventaris VALUES("16","Kursi Lab","Baru","Aset sekolah ini berasal dari dana bos yang digunakan siswa untuk belajar","80","6","2019-03-19","3","1004","1","0");
INSERT INTO inventaris VALUES("17","Sertifikat Tanah","Baru","Merupakan Bukti sertifikat tanah SMKN 1 Ciomas","3","7","2019-03-19","6","1005","1","0");
INSERT INTO inventaris VALUES("18","PC Acer","Lama","Aset sekolah ini berasal dari dana bos yang digunakan Staf Tata usaha untuk mendata administrasi","5","3","2019-03-19","5","1006","1","0");
INSERT INTO inventaris VALUES("19","Server","Lama","Aset sekolah ini berasal dari dana bos yang digunakan menyimpan data sekolah","4","5","2019-03-19","4","1007","1","0");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(100) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("2","KIB Gedung dan Bangunan","KOGB","Kartu Inventaris Barang Milik sekolah menyangkut gedung dan bangunan yang dimiliki sekolah berdasarkan bantuan negara","0");
INSERT INTO jenis VALUES("3","KIB Aset Tetap","KOOT","Milik Negara Indonesia","0");
INSERT INTO jenis VALUES("5","KIB Jalan Irigasi Dan Jaringan","KOOJ","Milik Negara","0");
INSERT INTO jenis VALUES("6","KIB Peralatan Dan Mesin","K0PM","Kartu Inventaris Barang Mengenai Peralatan Dan Mesin Milik Sekolah","0");
INSERT INTO jenis VALUES("7","KIB Tanah","K00T","Kartu Inventaris Barang Negara Berdasarkan Sertifikat Yang Menyangkut Tanah","0");
INSERT INTO jenis VALUES("8","nbh","hvhnhnn","vhv","1");



DROP TABLE kelas;

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `nm_kelas` varchar(50) NOT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","Administrator","0");
INSERT INTO level VALUES("2","Operator","0");
INSERT INTO level VALUES("3","Peminjam","0");



DROP TABLE log;

CREATE TABLE `log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `id_petugas` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

INSERT INTO log VALUES("1","Tambah Data","2019-04-01","6");
INSERT INTO log VALUES("2","Update Data","2019-04-01","6");
INSERT INTO log VALUES("3","Update Data","2019-04-01","5");
INSERT INTO log VALUES("4","Update Data","2019-04-01","1");
INSERT INTO log VALUES("5","Tambah Data","2019-04-01","7");
INSERT INTO log VALUES("6","Update Data","2019-04-01","7");
INSERT INTO log VALUES("7","Update Data","2019-04-01","7");
INSERT INTO log VALUES("8","Update Data","2019-04-02","5");
INSERT INTO log VALUES("9","Update Data","2019-04-02","5");
INSERT INTO log VALUES("10","Update Data","2019-04-02","6");
INSERT INTO log VALUES("11","Update Data","2019-04-02","7");
INSERT INTO log VALUES("12","Update Data","2019-04-02","5");
INSERT INTO log VALUES("13","Update Data","2019-04-02","3");
INSERT INTO log VALUES("14","Update Data","2019-04-02","3");
INSERT INTO log VALUES("15","Update Data","2019-04-02","3");
INSERT INTO log VALUES("16","Hapus Data","2019-04-03","3");
INSERT INTO log VALUES("17","Hapus Data","2019-04-03","4");
INSERT INTO log VALUES("18","Hapus Data","2019-04-03","5");
INSERT INTO log VALUES("19","Tambah Data","2019-04-03","6");
INSERT INTO log VALUES("20","Update Data","2019-04-03","6");
INSERT INTO log VALUES("21","Update Data","2019-04-04","1");
INSERT INTO log VALUES("22","Tambah Data","2019-04-04","7");
INSERT INTO log VALUES("23","Update Data","2019-04-04","7");
INSERT INTO log VALUES("24","Update Data","2019-04-04","7");
INSERT INTO log VALUES("25","Update Data","2019-04-04","1");
INSERT INTO log VALUES("26","Update Data","2019-04-07","1");
INSERT INTO log VALUES("27","Update Data","2019-04-07","1");
INSERT INTO log VALUES("28","Update Data","2019-04-07","1");
INSERT INTO log VALUES("29","Update Data","2019-04-07","1");
INSERT INTO log VALUES("30","Update Data","2019-04-07","1");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_kembali` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_peminjaman` varchar(50) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("15","2019-03-19 16:17:33","2019-03-19 10:03:33","Sudah Di Kembalikan","2","0");
INSERT INTO peminjaman VALUES("16","2019-03-18 19:53:04","2019-03-18 13:03:04","Sudah Di Kembalikan","5","0");
INSERT INTO peminjaman VALUES("17","2019-03-18 19:53:13","2019-03-18 13:03:13","Sudah Di Kembalikan","1","0");
INSERT INTO peminjaman VALUES("18","2019-03-19 16:17:26","2019-03-19 10:03:26","Sudah Di Kembalikan","5","0");
INSERT INTO peminjaman VALUES("19","2019-03-19 16:17:29","2019-03-19 10:03:29","Sudah Di Kembalikan","1","0");
INSERT INTO peminjaman VALUES("20","2019-03-19 16:40:35","2019-03-19 10:03:35","Sudah Di Kembalikan","5","0");
INSERT INTO peminjaman VALUES("21","2019-03-19 16:40:31","2019-03-19 10:03:31","Sudah Di Kembalikan","5","0");
INSERT INTO peminjaman VALUES("22","2019-03-19 16:42:03","2019-03-19 10:03:03","Sudah Di Kembalikan","5","0");
INSERT INTO peminjaman VALUES("23","2019-04-01 13:36:55","2019-04-01 08:04:55","Sudah Di Kembalikan","2","0");
INSERT INTO peminjaman VALUES("24","2019-03-31 10:47:30","0000-00-00 00:00:00","Sedang di Pinjam","5","0");
INSERT INTO peminjaman VALUES("25","2019-03-31 13:52:52","2019-03-31 08:03:52","Sudah Di Kembalikan","5","0");
INSERT INTO peminjaman VALUES("26","2019-03-31 13:59:05","0000-00-00 00:00:00","Sedang di Pinjam","5","0");
INSERT INTO peminjaman VALUES("27","2019-04-08 20:41:53","2019-04-08 15:04:53","Sudah Di Kembalikan","1","0");
INSERT INTO peminjaman VALUES("28","2019-04-08 20:41:59","2019-04-08 15:04:59","Sudah Di Kembalikan","1","0");
INSERT INTO peminjaman VALUES("29","2019-04-08 20:42:07","2019-04-08 15:04:06","Sudah Di Kembalikan","1","0");
INSERT INTO peminjaman VALUES("30","2019-04-08 20:38:40","0000-00-00 00:00:00","Sedang di Pinjam","1","0");
INSERT INTO peminjaman VALUES("31","2019-04-09 08:56:16","0000-00-00 00:00:00","Sedang di Pinjam","1","0");
INSERT INTO peminjaman VALUES("32","2019-04-09 08:57:13","0000-00-00 00:00:00","Sedang di Pinjam","1","0");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `token` varchar(10) NOT NULL,
  `id_level` int(10) unsigned NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT '0',
  `terhapus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin_sarpranas","7fa2ce33a8581e37a2670f020265299b","Elsa Noviani","elsa.noviani15@gmail.com","","1","1","0");
INSERT INTO petugas VALUES("2","operator_sarpranas","c055bda9a7fd66ecfd1b048382f1a381","Ridwan","dudyiskandar325@gmail.com","tK2yHSzE","2","1","0");
INSERT INTO petugas VALUES("6","Qilla","8a177652ac94c1a2e98103ae3f817e43","Elsa Noviani","qilla_q@gmail.com","83rudfsbj","3","1","0");
INSERT INTO petugas VALUES("7","Dini Nursafitri","750ce3dd414234db45af945bb2087eb5","Elsa Noviani","dininurs29@gmail.com","AN7bA03Z","3","1","0");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(100) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab Rpl 2","112","Baik","1");
INSERT INTO ruang VALUES("2","LAB RPL3","LB-RPL3","404 Not Found / ","1");
INSERT INTO ruang VALUES("3","Lab Rpl 2","L0R2","Barang Sekolah (Benda Elektronik)","0");
INSERT INTO ruang VALUES("4","Lab Rpl 1","L0R1","Penyimpanan Benda Elektronik RPL","0");
INSERT INTO ruang VALUES("5","Tata Usaha","T01U","Barang-barang milik sekolah yang disimpan oleh staf tata usaha SMKN 1 CIOMAS","0");
INSERT INTO ruang VALUES("6","Studio Animasi","S0A1","Barang-barang penyimpanan yang ada pada studio animasi","0");
INSERT INTO ruang VALUES("7","Bengkel TKR","B0TR","Barang-barang Penyimpanan milik jurusan Tkr seperti mesin dan alat-alat bengkel lainnya","0");
INSERT INTO ruang VALUES("8","Bengkel TPL","B0TL","Barang-barang Penyimpanan milik jurusan TPL seperti mesin dan alat-alat Las lainnya","0");
INSERT INTO ruang VALUES("9","Ruang Guru","RG01","Barang-barang Penyimpanan milik sekolah guna memberikan fasilitas kepada guru-guru dalam kegiatan KBM,Dan menyimpan arsip tugas siswa","0");



DROP TABLE siswa;

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL AUTO_INCREMENT,
  `nm_siswa` varchar(100) NOT NULL,
  `kode` char(15) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO siswa VALUES("1","Elsa Noviani","10001","Ds.SUkaharja");
INSERT INTO siswa VALUES("2","Dini Nursafitri","10002","Dramaga");



