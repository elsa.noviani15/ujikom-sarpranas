<?php if(isset($_SESSION['message'])){
  echo"<script>document.getElementById('showToast').click();</script>";
  unset($_SESSION['message']);
}?>
<div class="section card pt-0">
  <h4 class="card-header">Jenis Inventaris</h4>
  <div class="container mt-10">
    <button class="btn btn-custom" onclick="OpenModal('Jenis Inventaris - Form','page/jenis/form.php');"><i class="large mdi-content-add"></i> <span>Tambah</span></button>
    <div class="table-responsive" id="table-datatables">
      <table id="data-table-simple" class="display bordered" cellspacing="0">
        <thead>
          <tr>
            <th class="wd-44 center">No</th>
            <th>Nama Jenis</th>
            <th>Kode Jenis</th>
            <th>Keterangan</th>
            <th class="center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          $db= new Database();
          $select = $db->get_list('jenis');
          $select->bind_result($id, $nama_jenis, $kode_jenis,$keterangan,$terhapus);
          while($select->fetch())
          {
            ?>
            <tr>
             <td class="center"><?= $no++; ?></td>
             <td><?= $nama_jenis; ?></td>
             <td><?= $kode_jenis; ?></td>
             <td><?= $keterangan; ?></td>
             <!-- <td title="<?=$keterangan;?>"><?= substr($keterangan,0,30); ?>...</td> -->
             <td class="center"> 
               <a href="#" onclick="OpenModal('Jenis Inventaris - Form','page/jenis/form.php?id=<?=$id;?>')"><i class="mdi-image-edit blue-text"></i></a>
               <a href="#" onclick="showDialogDelete('page/jenis/proses.php?id=<?=$id;?>')"><i class="mdi-action-delete red-text"></i></a>
             </td>
           </tr>
           <?php
         }
         ?>
       </tbody>
     </table>
   </div>
 </div>