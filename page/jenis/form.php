<?php
include "../Database.php";
$id=isset($_GET['id'])?$_GET['id']:null;
$db=new Database();
if(isset($id)){
  $select=$db->get_by_id('jenis',$id);
  $select->bind_result($id, $nama_jenis, $kode_jenis,$keterangan,$terhapus);
  $select->fetch();
}
?>
<form id="formModal" method="POST" action="./page/jenis/proses.php" class="col s12">
  <div class="row">
    <div class="input-field col s12">
      <?=isset($id)?'<input type="hidden" name="id" value="'.$id.'">':'';?>
      <input type="text" name="nama_jenis" class="validate" required="" value="<?=isset($nama_jenis)?$nama_jenis:'';?>" autofocus>
      <label for="nama_jenis" class="<?=isset($nama_jenis)?'active':'';?>">Nama Jenis</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="text" name="kode_jenis" length="50" class="validate" value="<?=isset($kode_jenis)?$kode_jenis:'';?>" required="">
      <label for="kode_jenis" class="<?=isset($kode_jenis)?'active':'';?>">Kode Jenis</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <textarea name="keterangan" class="materialize-textarea validate"><?=isset($keterangan)?$keterangan:'';?></textarea>
      <label for="keterangan" class="<?=isset($keterangan)?'active':'';?>">Keterangan</label>
    </div>
  </div>
  <div class="row">
  	<div class="col m12">
  		<input type="submit" name="submit" value="Simpan" class="btn blue right ml-10">
  		<button type="button" href="#" class="btn red right" onclick="CloseModal()">Cancel</button>
  	</div>
  </div>
</form>