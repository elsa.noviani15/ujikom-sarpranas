<?php if(isset($_SESSION['message'])){
  echo"<script>document.getElementById('showToast').click();</script>";
  unset($_SESSION['message']);
}?>
<div class="section card pt-0">
  <h4 class="card-header">Enumeration</h4>
  <div class="container mt-10">
    <button class="btn btn-custom" onclick="OpenModal('Enumeration - Form','page/Enumeration/form.php');"><i class="large mdi-content-add"></i> <span>Tambah</span></button>
    <div class="table-responsive" id="table-datatables">
      <table id="data-table-simple" class=" display bordered" cellspacing="0">
        <thead>
          <tr>
            <th class="wd-44 center">No</th>
            <th>Key</th>
            <th>Value</th>
            <th class="center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          $db= new Database();
          $select = $db->get_list('enum');
          $select->bind_result($id, $key, $value,$terhapus);
          while($select->fetch())
          {
            ?>
            <tr>
             <td class="center"><?= $no++; ?></td>
             <td><?= $key; ?></td>
             <td><?= $value; ?></td>
             <td class="center"> 
               <a href="#" onclick="OpenModal('Enumeration - Form','page/Enumeration/form.php?id=<?=$id;?>')"><i class="mdi-image-edit blue-text"></i></a>
               <a href="#" onclick="showDialogDelete('page/Enumeration/proses.php?id=<?=$id;?>')"><i class="mdi-action-delete red-text"></i></a>
             </td>
           </tr>
           <?php
         }
         ?>
       </tbody>
     </table>
   </div>
 </div>