<?php
include "../Database.php";
$id=isset($_GET['id'])?$_GET['id']:null;
$db=new Database();
if(isset($id)){
  $select=$db->get_by_id('enum',$id);
  $select->bind_result($id, $key, $value,$terhapus);
  $select->fetch();
}
?>
<form id="formModal" method="POST" action="./page/Enumeration/proses.php" class="col s12">
  <div class="row">
    <div class="input-field col s12">
      <?=isset($id)?'<input type="hidden" name="id" value="'.$id.'">':'';?>
      <input type="text" name="key" class="validate" required="" value="<?=isset($key)?$key:'';?>" autofocus>
      <label for="key" class="<?=isset($key)?'active':'';?>">Key</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="text" name="value" class="validate" length="120" value="<?=isset($value)?$value:'';?>" required="">
      <label for="value" class="<?=isset($value)?'active':'';?>">Value</label>
    </div>
  </div>
  <div class="row">
  	<div class="col m12">
  		<input type="submit" name="submit" value="Simpan" class="btn blue right ml-10">
  		<button type="button" href="#" class="btn red right" onclick="CloseModal()">Cancel</button>
  	</div>
  </div>
</form>