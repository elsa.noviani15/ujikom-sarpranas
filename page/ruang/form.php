<?php
include "../Database.php";
$id=isset($_GET['id'])?$_GET['id']:null;
$db=new Database();
if(isset($id)){
  $select=$db->get_by_id('ruang',$id);
  $select->bind_result($id, $nama_ruang, $kode_ruang,$keterangan,$terhapus);
  $select->fetch();
}
?>
<form id="formModal" method="POST" action="./page/ruang/proses.php" class="col s12">
  <div class="row">
    <div class="input-field col s12">
      <?=isset($id)?'<input type="hidden" name="id" value="'.$id.'">':'';?>
      <input type="text" name="nama_ruang" class="validate" required="" value="<?=isset($nama_ruang)?$nama_ruang:'';?>" autofocus maxlength="50">
      <label for="nama_ruang" class="<?=isset($nama_ruang)?'active':'';?>">Nama Ruang</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="text" name="kode_ruang" length="5" class="validate" value="<?=isset($kode_ruang)?$kode_ruang:'';?>" required="" >
      <label for="kode_ruang" class="<?=isset($kode_ruang)?'active':'';?>">Kode Ruang</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <textarea name="keterangan" class="materialize-textarea validate"><?=isset($keterangan)?$keterangan:'';?></textarea>
      <label for="keterangan" class="<?=isset($keterangan)?'active':'';?>">Keterangan</label>
    </div>
  </div>
  <div class="row">
  	<div class="col m12">
  		<input type="submit" name="submit" value="Simpan" class="btn blue right ml-10">
  		<button type="button" href="#" class="btn red right" onclick="CloseModal()">Cancel</button>
  	</div>
  </div>
</form>