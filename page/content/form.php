<?php
include "../Database.php";
$id=isset($_GET['id'])?$_GET['id']:null;
$db=new Database();
if(isset($id)){
  $select=$db->get_by_id('content',$id);
  $select->bind_result($id, $judul, $foto,$content,$tanggal,$terhapus);
  $select->fetch();
}
?>
<form id="formModal" method="POST" action="./page/content/proses.php" class="col s12">
  <div class="row">
    <div class="input-field col s12">
      <?=isset($id)?'<input type="hidden" name="id" value="'.$id.'">':'';?>
      <input type="text" name="judul" class="validate" required="" value="<?=isset($judul)?$judul:'';?>" autofocus>
      <label for="judul" class="<?=isset($judul)?'active':'';?>">Judul</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="text" name="foto" class="validate" length="300" content="<?=isset($foto)?$foto:'';?>" required="">
      <label for="foto" class="<?=isset($foto)?'active':'';?>">Foto</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <input type="text" name="content" class="validate" length="300" content="<?=isset($content)?$content:'';?>" required="">
      <label for="content" class="<?=isset($content)?'active':'';?>">Content</label>
    </div>
  </div>
  <div class="row">
  	<div class="col m12">
  		<input type="submit" name="submit" value="Simpan" class="btn blue right ml-10">
  		<button type="button" href="#" class="btn red right" onclick="CloseModal()">Cancel</button>
  	</div>
  </div>
</form>