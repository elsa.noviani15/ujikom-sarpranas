<?php if(isset($_SESSION['message'])){
  echo"<script>document.getElementById('showToast').click();</script>";
  unset($_SESSION['message']);
}?>
<div class="section card pt-0">
  <h4 class="card-header">Content</h4>
  <div class="container mt-10">
    <button class="btn btn-custom" onclick="OpenModal('Content - Form','page/content/form.php');"><i class="large mdi-content-add"></i> <span>Tambah</span></button>
    <div class="table-responsive" id="table-datatables">
      <table id="data-table-simple" class=" display bordered" cellspacing="0">
        <thead>
          <tr>
            <th class="wd-44 center">No</th>
            <th>Judul</th>
            <th>Foto</th>
            <th>Content</th>
            <th>Tanggal</th>
            <th class="center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          $db= new Database();
          $select = $db->get_list('content');
          $select->bind_result($id, $judul, $foto,$content,$tanggal,$terhapus);
          while($select->fetch())
          {
            ?>
            <tr>
             <td class="center"><?= $no++; ?></td>
             <td><?= $judul; ?></td>
             <td><?= $foto; ?></td>
             <td><?= substr($content,0,100);?></td>
             <td><?= $tanggal; ?></td>
             <td class="center"> 
               <a href="#" onclick="OpenModal('Content - Form','page/content/form.php?id=<?=$id;?>')"><i class="mdi-image-edit blue-text"></i></a>
               <a href="#" onclick="showDialogDelete('page/content/proses.php?id=<?=$id;?>')"><i class="mdi-action-delete red-text"></i></a>
             </td>
           </tr>
           <?php
         }
         ?>
       </tbody>
     </table>
   </div>
 </div>