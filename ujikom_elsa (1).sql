-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2019 at 09:54 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom_elsa`
--

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id_content` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `tanggal` date NOT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id_content`, `judul`, `foto`, `content`, `tanggal`, `terhapus`) VALUES
(1, 'SMKN 1 CIOMAS', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n', '2019-02-04', 0),
(2, 'hsah', 'DSBHSA', 'SDHAB', '0000-00-00', 0),
(3, 'dsjs', 'asdnj', 'sjdna', '0000-00-00', 0),
(4, 'sac', 'sca', 'sa', '0000-00-00', 0),
(5, 'sdf', 'dsf', 'sdf', '0000-00-00', 1),
(6, 'asdadsa', 'asdas', 'asdas', '0000-00-00', 0),
(7, 'asd', 'asdas', 'asdas', '0000-00-00', 1),
(8, 'asdas', 'asdas', 'asdasd', '2019-02-19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `jumlah` int(30) NOT NULL,
  `tanggal_kembali` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `id_peminjaman`, `jumlah`, `tanggal_kembali`, `terhapus`) VALUES
(18, 13, 15, 1, '2019-03-19 03:03:33', 0),
(19, 14, 15, 31, '2019-03-19 03:03:33', 0),
(20, 13, 16, 1, '2019-03-18 06:03:04', 0),
(21, 14, 16, 1, '2019-03-18 06:03:04', 0),
(22, 13, 17, 1, '2019-03-18 06:03:13', 0),
(23, 14, 18, 1, '2019-03-19 03:03:26', 0),
(24, 14, 19, 26, '2019-03-19 03:03:29', 0),
(25, 13, 19, 1, '2019-03-19 03:03:29', 0),
(26, 14, 20, 2, '2019-03-19 03:03:35', 0),
(27, 14, 21, 3, '2019-03-19 03:03:31', 0),
(28, 14, 22, 30, '2019-03-19 03:03:03', 0),
(29, 14, 23, 4, '2019-04-01 01:04:55', 0),
(30, 14, 24, 1, NULL, 0),
(31, 14, 25, 1, '2019-03-31 01:03:52', 0),
(32, 14, 26, 4, NULL, 0),
(33, 17, 26, 5, NULL, 0),
(34, 14, 27, 1, NULL, 0),
(35, 14, 28, 1, NULL, 0),
(36, 13, 29, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kondisi` varchar(50) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`, `terhapus`) VALUES
(13, 'Proyektor', 'Baru', 'Alat ini digunakan untuk menampilkan informasi pada saat presentasi agar penyampaiannya menarik', 10, 6, '2019-03-18', 5, 1001, 1, 0),
(14, 'Meja Lab RPL', 'Baru', 'Barang ini milik jurusan RPL berdasarkan dana bantuan BOS untuk keperluan KBM RPL', 63, 6, '2019-03-18', 4, 1002, 1, 0),
(15, 'Cable VGA', 'Baru', 'Alat ini digunakan untuk menyambungkan proyektor dengan laptop', 40, 6, '2019-03-19', 5, 1003, 1, 0),
(16, 'Kursi Lab', 'Baru', 'Aset sekolah ini berasal dari dana bos yang digunakan siswa untuk belajar', 80, 6, '2019-03-19', 3, 1004, 1, 0),
(17, 'Sertifikat Tanah', 'Baru', 'Merupakan Bukti sertifikat tanah SMKN 1 Ciomas', 3, 7, '2019-03-19', 6, 1005, 1, 0),
(18, 'PC Acer', 'Lama', 'Aset sekolah ini berasal dari dana bos yang digunakan Staf Tata usaha untuk mendata administrasi', 5, 3, '2019-03-19', 5, 1006, 1, 0),
(19, 'Server', 'Lama', 'Aset sekolah ini berasal dari dana bos yang digunakan menyimpan data sekolah', 4, 5, '2019-03-19', 4, 1007, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`, `terhapus`) VALUES
(2, 'KIB Gedung dan Bangunan', 'KOGB', 'Kartu Inventaris Barang Milik sekolah menyangkut gedung dan bangunan yang dimiliki sekolah berdasarkan bantuan negara', 0),
(3, 'KIB Aset Tetap', 'KOOT', 'Milik Negara Indonesia', 0),
(5, 'KIB Jalan Irigasi Dan Jaringan', 'KOOJ', 'Milik Negara', 0),
(6, 'KIB Peralatan Dan Mesin', 'K0PM', 'Kartu Inventaris Barang Mengenai Peralatan Dan Mesin Milik Sekolah', 0),
(7, 'KIB Tanah', 'K00T', 'Kartu Inventaris Barang Negara Berdasarkan Sertifikat Yang Menyangkut Tanah', 0),
(8, 'nbh', 'hvhnhnn', 'vhv', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `nm_kelas` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(30) NOT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`, `terhapus`) VALUES
(1, 'Administrator', 0),
(2, 'Operator', 0),
(3, 'Peminjam', 0);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id_log` int(11) NOT NULL,
  `action` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `id_petugas` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id_log`, `action`, `tanggal`, `id_petugas`) VALUES
(1, 'Tambah Data', '2019-04-01', 6),
(2, 'Update Data', '2019-04-01', 6),
(3, 'Update Data', '2019-04-01', 5),
(4, 'Update Data', '2019-04-01', 1),
(5, 'Tambah Data', '2019-04-01', 7),
(6, 'Update Data', '2019-04-01', 7),
(7, 'Update Data', '2019-04-01', 7),
(8, 'Update Data', '2019-04-02', 5),
(9, 'Update Data', '2019-04-02', 5),
(10, 'Update Data', '2019-04-02', 6),
(11, 'Update Data', '2019-04-02', 7),
(12, 'Update Data', '2019-04-02', 5),
(13, 'Update Data', '2019-04-02', 3),
(14, 'Update Data', '2019-04-02', 3),
(15, 'Update Data', '2019-04-02', 3),
(16, 'Hapus Data', '2019-04-03', 3),
(17, 'Hapus Data', '2019-04-03', 4),
(18, 'Hapus Data', '2019-04-03', 5),
(19, 'Tambah Data', '2019-04-03', 6),
(20, 'Update Data', '2019-04-03', 6),
(21, 'Update Data', '2019-04-04', 1),
(22, 'Tambah Data', '2019-04-04', 7),
(23, 'Update Data', '2019-04-04', 7),
(24, 'Update Data', '2019-04-04', 7),
(25, 'Update Data', '2019-04-04', 1),
(26, 'Update Data', '2019-04-07', 1),
(27, 'Update Data', '2019-04-07', 1),
(28, 'Update Data', '2019-04-07', 1),
(29, 'Update Data', '2019-04-07', 1),
(30, 'Update Data', '2019-04-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `tanggal_pinjam` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_kembali` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_peminjaman` varchar(50) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_petugas`, `terhapus`) VALUES
(15, '2019-03-19 09:17:33', '2019-03-19 03:03:33', 'Sudah Di Kembalikan', 2, 0),
(16, '2019-03-18 12:53:04', '2019-03-18 06:03:04', 'Sudah Di Kembalikan', 5, 0),
(17, '2019-03-18 12:53:13', '2019-03-18 06:03:13', 'Sudah Di Kembalikan', 1, 0),
(18, '2019-03-19 09:17:26', '2019-03-19 03:03:26', 'Sudah Di Kembalikan', 5, 0),
(19, '2019-03-19 09:17:29', '2019-03-19 03:03:29', 'Sudah Di Kembalikan', 1, 0),
(20, '2019-03-19 09:40:35', '2019-03-19 03:03:35', 'Sudah Di Kembalikan', 5, 0),
(21, '2019-03-19 09:40:31', '2019-03-19 03:03:31', 'Sudah Di Kembalikan', 5, 0),
(22, '2019-03-19 09:42:03', '2019-03-19 03:03:03', 'Sudah Di Kembalikan', 5, 0),
(23, '2019-04-01 06:36:55', '2019-04-01 01:04:55', 'Sudah Di Kembalikan', 2, 0),
(24, '2019-03-31 03:47:30', '0000-00-00 00:00:00', 'Sedang di Pinjam', 5, 0),
(25, '2019-03-31 06:52:52', '2019-03-31 01:03:52', 'Sudah Di Kembalikan', 5, 0),
(26, '2019-03-31 06:59:05', '0000-00-00 00:00:00', 'Sedang di Pinjam', 5, 0),
(27, '2019-03-31 10:09:50', '0000-00-00 00:00:00', 'Sedang di Pinjam', 1, 0),
(28, '2019-04-07 07:07:45', '0000-00-00 00:00:00', 'Sedang di Pinjam', 1, 0),
(29, '2019-04-07 07:30:03', '0000-00-00 00:00:00', 'Sedang di Pinjam', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `token` varchar(10) NOT NULL,
  `id_level` int(10) UNSIGNED NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT '0',
  `terhapus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `email`, `token`, `id_level`, `aktif`, `terhapus`) VALUES
(1, 'admin_sarpranas', '7fa2ce33a8581e37a2670f020265299b', 'Elsa Noviani', 'elsa.noviani15@gmail.com', '', 1, 1, 0),
(2, 'operator_sarpranas', 'c055bda9a7fd66ecfd1b048382f1a381', 'Ridwan', 'dudyiskandar325@gmail.com', 'tK2yHSzE', 2, 1, 0),
(6, 'Qilla', '8a177652ac94c1a2e98103ae3f817e43', 'Elsa Noviani', 'qilla_q@gmail.com', '83rudfsbj', 3, 1, 0),
(7, 'Dini Nursafitri', '750ce3dd414234db45af945bb2087eb5', 'Elsa Noviani', 'dininurs29@gmail.com', 'AN7bA03Z', 3, 1, 0);

--
-- Triggers `petugas`
--
DELIMITER $$
CREATE TRIGGER `Insert_sign` AFTER INSERT ON `petugas` FOR EACH ROW INSERT INTO log VALUES("","Tambah Data",now(),new.id_petugas)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Update_sign` BEFORE UPDATE ON `petugas` FOR EACH ROW INSERT INTO log VALUES("","Update Data",now(),old.id_petugas)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `delete_sign` BEFORE DELETE ON `petugas` FOR EACH ROW INSERT INTO log VALUES("","Hapus Data",now(),old.id_petugas)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(100) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `terhapus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`, `terhapus`) VALUES
(1, 'Lab Rpl 2', '112', 'Baik', 1),
(2, 'LAB RPL3', 'LB-RPL3', '404 Not Found / ', 1),
(3, 'Lab Rpl 2', 'L0R2', 'Barang Sekolah (Benda Elektronik)', 0),
(4, 'Lab Rpl 1', 'L0R1', 'Penyimpanan Benda Elektronik RPL', 0),
(5, 'Tata Usaha', 'T01U', 'Barang-barang milik sekolah yang disimpan oleh staf tata usaha SMKN 1 CIOMAS', 0),
(6, 'Studio Animasi', 'S0A1', 'Barang-barang penyimpanan yang ada pada studio animasi', 0),
(7, 'Bengkel TKR', 'B0TR', 'Barang-barang Penyimpanan milik jurusan Tkr seperti mesin dan alat-alat bengkel lainnya', 0),
(8, 'Bengkel TPL', 'B0TL', 'Barang-barang Penyimpanan milik jurusan TPL seperti mesin dan alat-alat Las lainnya', 0),
(9, 'Ruang Guru', 'RG01', 'Barang-barang Penyimpanan milik sekolah guna memberikan fasilitas kepada guru-guru dalam kegiatan KBM,Dan menyimpan arsip tugas siswa', 0);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `nm_siswa` varchar(100) NOT NULL,
  `kode` char(15) NOT NULL,
  `alamat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nm_siswa`, `kode`, `alamat`) VALUES
(1, 'Elsa Noviani', '10001', 'Ds.SUkaharja'),
(2, 'Dini Nursafitri', '10002', 'Dramaga');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id_content`);

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id_content` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
