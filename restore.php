<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Restore Database</title>
    <link rel="stylesheet" href="css/materialize.min.css">
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
    <script src="js/materialize.min.js"></script>
</head>
<body>
    <div class="row">
        <div class="col s12 m4 offset-m4 center" style="margin-top:70px">
            <div class="file-field input-field">
                <h2>Restore Database</h2>
                <form action="">
                    <div class="btn">
                        <span>File</span>
                        <input type="file" required>
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" required>
                    </div>
                    <button class="btn col s12 m12s" type="submit">Submit</button> 
                </form>
            </div>

        </div>
    </div>
</body>
</html>