<?php
include 'page/Database.php';
  $db=new Database();
  // $db->loading();
  if(isset($_POST['submit'])){
    $token=$db->randomString();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/Sarpranas_EN2019";
    $penerima=$_POST['email'];
    $judul="Forgot Password - Sarpranas";
    $isi="<p>Ini Token : ".$token." <a href='".$actual_link."?mode=ubah-password&token=".$token."' >Klik disini</a>. Token ini hanya dapat digunakan 1 kali</p>";
      $q=$db->get_where("petugas","email",$penerima);
      $isvalid=mysqli_num_rows($q);
      if($isvalid > 0){
          $send=$db->send_email($judul,$isi,$penerima);
          if($send=='Berhasil'){
              $user=mysqli_fetch_assoc($q);
              $query="UPDATE petugas SET token='$token' WHERE id_petugas=$user[id_petugas] AND terhapus=0";
                if($db->query($query)){
                  $db->back('Silahkan Cek Email Anda.');
                }else{
                  $db->back('Gagal Perbaharui Token');
                }
          }else{
            $db->back('Gagal Kirim Email');
          }
      }else{
            $db->back('Email Tidak Di temukan');
      }
  }
  if(isset($_POST['gantiPassword'])){
    $query_check_token=$db->get_where("petugas","token",$_POST['token']);
    $isValid=mysqli_num_rows($query_check_token);
    if($isValid >0){
      if($_POST['NewPassword']==$_POST['ReTypePassword']){
        $data=mysqli_fetch_assoc($query_check_token);
        $password=md5($_POST['NewPassword']);
        $query_update_password=$db->update("petugas","password",$password,$data['id_petugas']);
        if($query_update_password){
          $query_empty_token=$db->update("petugas","token",null,$data['id_petugas']);
          $db->back("Password Anda Telah Berhasil di Reset","reset");
        }else{
          $db->back("Error saat ubah password");
        }
      }else{
        echo"Password not Equal";
        $db->back("Password Baru dan Re-Type Password Tidak Cocok","mode=ubah-password&token=".$_POST['token']);
      }
    }else{
      $db->back("Token Yang Anda Masukan Salah");
    }
  }